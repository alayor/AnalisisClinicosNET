﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;

namespace AnalisisClinicos
{
    public partial class F0302 : Form
    {
        BindingList<Paciente> lsPacientes;
        static internal int idPaciente;


        public F0302()
        {
            try
            {
                InitializeComponent();
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    obtenerPacientes();

                }

                tbNombre.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbApellidoPaterno_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    obtenerPacientes();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbApellidoMaterno_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    obtenerPacientes();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btModificar_Click(object sender, EventArgs e)
        {
            try
            {
                modificar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                idPaciente = 0;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPaciente.Rows.Count > 0 && dgvPaciente.CurrentCell.RowIndex >= 0)
                {

                    idPaciente = (int)lsPacientes[dgvPaciente.CurrentCell.RowIndex].idPaciente;

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0302_Load(object sender, EventArgs e)
        {
            try{
                dgvPaciente.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular);
                tbClave.Select();
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        internal void obtenerPacientes()
        {
            try
            {
                if (tbNombre.Text != "" || tbClave.Text != "")
                {
                    Paciente paciente = new Paciente();
                    paciente.nombre = tbNombre.Text;
                    paciente.clave = tbClave.Text;

                    lsPacientes = PacienteBR.listarPorNombreClave(paciente);

                    FDataGridView.Llenar(dgvPaciente, lsPacientes, "clave",
                    "nombre", "apellidoPaterno", "apellidoMaterno", "numeroAfiliacion", "fechaNacimiento", "fechaIngreso");

                    dgvPaciente.Columns["idPaciente"].Width = 0;

                    dgvPaciente.Columns["clave"].DisplayIndex = 0;
                    dgvPaciente.Columns["clave"].Width = 20;
                    dgvPaciente.Columns["clave"].HeaderText = "Clave";

                    dgvPaciente.Columns["numeroAfiliacion"].DisplayIndex = 1;
                    dgvPaciente.Columns["numeroAfiliacion"].Width = 20;
                    dgvPaciente.Columns["numeroAfiliacion"].HeaderText = "No. Afiliación";

                    dgvPaciente.Columns["nombre"].DisplayIndex = 2;
                    dgvPaciente.Columns["nombre"].Width = 30;
                    dgvPaciente.Columns["nombre"].HeaderText = "Nombre";

                    dgvPaciente.Columns["apellidoPaterno"].DisplayIndex =3;
                    dgvPaciente.Columns["apellidoPaterno"].Width = 25;
                    dgvPaciente.Columns["apellidoPaterno"].HeaderText = "Apellido Paterno";

                    dgvPaciente.Columns["apellidoMaterno"].DisplayIndex = 4;
                    dgvPaciente.Columns["apellidoMaterno"].Width = 25;
                    dgvPaciente.Columns["apellidoMaterno"].HeaderText = "Apellido Materno";


                    dgvPaciente.Columns["fechaNacimiento"].DisplayIndex = 5;
                    dgvPaciente.Columns["fechaNacimiento"].Width = 15;
                    dgvPaciente.Columns["fechaNacimiento"].HeaderText = "Nacimiento";

                    dgvPaciente.Columns["fechaIngreso"].DisplayIndex = 6;
                    dgvPaciente.Columns["fechaIngreso"].Width = 75;
                    dgvPaciente.Columns["fechaIngreso"].HeaderText = "Ingreso";
                }

                
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void modificar()
        {
            try
            {
                if (dgvPaciente.Rows.Count > 0 && dgvPaciente.CurrentCell.RowIndex >= 0)
                {
                    idPaciente = (int)lsPacientes[dgvPaciente.CurrentCell.RowIndex].idPaciente;

                    F0202 form = new F0202(idPaciente);
                    form.accionAlGuardarPrestamo = F0202.accion.CerrarVentana;
                    form.idPaciente = idPaciente;
                    form.btEliminar.Visible = true;
                    form.btBuscar.Visible = false;
                    form.bNuevo.Visible = false;
                    form.llCoincidencias.Visible = false;
                    form.ShowDialog();
                    obtenerPacientes();
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void tbClave_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    obtenerPacientes();

                }
                tbClave.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




      
    }
}
