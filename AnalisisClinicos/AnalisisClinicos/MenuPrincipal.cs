﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AnalisisClinicos
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {
            try
            {
                InicioSesion iniSes = new InicioSesion();

                iniSes.ShowDialog();

                tsslFecha.Text = DateTime.Now.ToLongDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0101ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //Catalogo - Categorias
            try
            {
                F0101 form = new F0101();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0102ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Catalogo - Grupos
            try
            {
                F0102 form = new F0102();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void análisisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Catalogo - Analisis
            try
            {
                F0103 form = new F0103();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MenuPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (MessageBox.Show("¿Desea cerrar la aplicación?", "Cerrar aplicación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }

        private void pacientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Proceso - Paciente
            try
            {
                F0202 form = new F0202();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pacientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //Consulta - Paciente
            try
            {
                F0302 form = new F0302();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ingresarResultadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Proceso - Resultado
            try
            {
                F0201 form = new F0201();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void analisisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Consulta - Analisis
            try
            {
                F0303 form = new F0303();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void resultadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Consulta - Resultados
            try
            {
                F0301 form = new F0301();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cambiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Seguridad - Cambio de contraseña
                F0501 form = new F0501();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void respaldarInformaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Seguridad - Respaldo
                F0502 form = new F0502();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ayudaDelSistemaDeAnálisisClínicosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(@"docs\manual.pdf");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Acerca de
                AcercaDe form = new AcercaDe();
                form.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //Catalogo - Tipo de paciente
            F0104 form = new F0104();
            form.Show();
        }

        private void resultadosPorMesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Reporte - Resultados por mes
            F0401 form = new F0401();
            form.Show();
        }

    }
}
