﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;

namespace AnalisisClinicos
{
    public partial class F0202 : Form
    {
        Paciente paciente = new Paciente();
        internal int idPaciente;
        internal accion accionAlGuardarPrestamo = accion.NoCerrarVentana;
        
        internal enum accion
        {
            NoCerrarVentana,
            CerrarVentana
        }


        public F0202()
        {
            try
            {
                InitializeComponent();
                nuevo();
                llenarCombo();
                btEliminar.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public F0202(int idPaciente):this()
        {
            try
            {
                this.idPaciente = idPaciente;
                obtenerPaciente(idPaciente);
                btEliminar.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0202_Load(object sender, EventArgs e)
        {
            try
            {
                
             }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void bGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (idPaciente == 0)
                {
                    guardar();
                }
                else
                {
                    actualizar();
                   
                }

               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                eliminar();
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNombre_TextChanged(object sender, EventArgs e)
        {
            try
            {
                buscarCoincidencias();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buscarCoincidencias()
        {
            try
            {


                if (tbNombre.Text != "" || tbApellidoPaterno.Text != "" || tbApellidoMaterno.Text != "" || tbClave.Text != "")
                {
                    
                paciente.nombre = tbNombre.Text;
                paciente.apellidoPaterno = tbApellidoPaterno.Text;
                paciente.apellidoMaterno = tbApellidoMaterno.Text;


                BindingList<Paciente> lsPaciente = new BindingList<Paciente>();

                lsPaciente = PacienteBR.listarPorNombreClave(paciente);

                if (lsPaciente.Count > 0 && lsPaciente.Count < 6)
                {
                    llCoincidencias.Text = lsPaciente.Count.ToString() + " coincidencia(s)";
                }
                else
                {
                    llCoincidencias.Text = "";
                }
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void bNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbApellidoPaterno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                buscarCoincidencias();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbApellidoMaterno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                buscarCoincidencias();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }       

        private void btBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                buscar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void llCoincidencias_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                mostrarCoincidencias();

             }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNumero_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (idPaciente == 0 )
                    bGuardar.Text = "&Guardar";
                else
                    bGuardar.Text = "&Actualizar";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

       


        private void nuevo()
        {
            try
            {

                idPaciente =0;
                tbNombre.ResetText();
                tbApellidoPaterno.ResetText();
                tbApellidoMaterno.ResetText();
                lbFechaIngreso.Text = DateTime.Now.Date.ToShortDateString();
                dtpFechaNacimiento.Value = DateTime.Now;
                mtbTelefonoLada.ResetText();
                mtbTelefono.ResetText();
                rbHombre.Checked = false;
                rbMujer.Checked = false;
                llCoincidencias.Visible = true;
                llCoincidencias.Text = "";
                cbTipoPaciente.SelectedItem = null;
                tbClave.ResetText();
                tbClave.Select();
                tbNumeroAfiliacion.ResetText();

                bGuardar.Text = "&Guardar";
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }

        }

        private void llenarCombo()
        {
            try
            {
                cbTipoPaciente.DisplayMember = "nombre";
                cbTipoPaciente.ValueMember = "idTipoPaciente";
                cbTipoPaciente.DataSource = TipoPacienteBR.listar();
                cbTipoPaciente.SelectedItem = null;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void guardar()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(tbClave.Text))
                {
                    MessageBox.Show("Ingrese al clave del paciente.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbClave.Select();
                    return;
                }
                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre del paciente.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }
                if (cbTipoPaciente.SelectedValue == null)
                {
                    MessageBox.Show("Seleccione el tipo de paciente", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbTipoPaciente.Select();
                    return;
                }

                Paciente paciente_existe = new Paciente();
                paciente_existe.clave = tbClave.Text.ToUpper();

                if (PacienteBR.existePorClave(paciente_existe))
                {
                    MessageBox.Show("La clave ya existe para paciente.", "Clave usada", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbClave.Select();
                    return;
                }



                paciente.idPaciente = idPaciente;
                paciente.clave = tbClave.Text;
                paciente.idTipoPaciente = Int32.Parse(cbTipoPaciente.SelectedValue.ToString());
                paciente.nombre = tbNombre.Text;
                paciente.apellidoPaterno = tbApellidoPaterno.Text;
                paciente.apellidoMaterno = tbApellidoMaterno.Text;
                paciente.fechaIngreso = DateTime.Now;
                paciente.telefono = mtbTelefonoLada.Text + "-" + mtbTelefono.Text;
                paciente.fechaNacimiento = dtpFechaNacimiento.Value.Date;
                paciente.genero = rbHombre.Checked ? "H" : rbMujer.Checked ? "M" : "";
                paciente.numeroAfiliacion = tbNumeroAfiliacion.Text;

                PacienteBR.insertar(paciente);

                if (paciente.idPaciente != 0)
                {
                    MessageBox.Show("El paciente ha sido registrado.", "Registro de paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   // nuevo();

                    if (accionAlGuardarPrestamo == accion.CerrarVentana)
                    {
                        this.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void obtenerPaciente(int idPaciente)
        {
            try
            {
                Paciente paciente_existe = new Paciente();
                paciente_existe.idPaciente = idPaciente;
                if (PacienteBR.existe(paciente_existe))
                {
                    tbClave.Text = paciente_existe.clave;
                    cbTipoPaciente.SelectedValue = paciente_existe.idTipoPaciente;
                    idPaciente = paciente_existe.idPaciente;
                    lbFechaIngreso.Text = paciente_existe.fechaIngreso.ToShortDateString();
                    tbNombre.Text = paciente_existe.nombre;
                    tbApellidoPaterno.Text = paciente_existe.apellidoPaterno;
                    tbApellidoMaterno.Text = paciente_existe.apellidoMaterno;
                    mtbTelefonoLada.Text = paciente_existe.telefono.Split('-')[0];
                    mtbTelefono.Text = paciente_existe.telefono.Split('-')[1];
                    dtpFechaNacimiento.Value = paciente_existe.fechaNacimiento;
                    rbHombre.Checked = paciente_existe.genero == "H" ? true : false;
                    rbMujer.Checked = paciente_existe.genero == "M" ? true : false;
                    tbNumeroAfiliacion.Text = paciente_existe.numeroAfiliacion;

                    bGuardar.Text = "&Actualizar";
                }



            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void actualizar()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(tbClave.Text))
                {
                    MessageBox.Show("Ingrese al clave del paciente.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbClave.Select();
                    return;
                }
                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre del paciente", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }
                if (cbTipoPaciente.SelectedValue==null)
                {
                    MessageBox.Show("Seleccione el tipo de paciente", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbTipoPaciente.Select();
                    return;
                }

                Paciente paciente_existe = new Paciente();
                paciente_existe.clave = tbClave.Text.ToUpper();

                if (PacienteBR.existePorClave(paciente_existe) && paciente_existe.idPaciente != idPaciente)
                {
                    MessageBox.Show("La clave ya existe para otro paciente.", "Clave usada", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbClave.Select();
                    return;
                }


                Paciente paciente = new Paciente();
                paciente.clave = tbClave.Text;
                paciente.idPaciente = idPaciente;
                paciente.idTipoPaciente = Int32.Parse(cbTipoPaciente.SelectedValue.ToString());
                paciente.nombre = tbNombre.Text;
                paciente.apellidoPaterno = tbApellidoPaterno.Text;
                paciente.apellidoMaterno = tbApellidoMaterno.Text;
                paciente.telefono = mtbTelefonoLada.Text + "-" + mtbTelefono.Text;
                paciente.fechaNacimiento = dtpFechaNacimiento.Value.Date;
                paciente.genero = rbHombre.Checked ? "H" : rbMujer.Checked ? "M" : "";
                paciente.numeroAfiliacion = tbNumeroAfiliacion.Text;


                int res = PacienteBR.actualizar(paciente);

                if (res != 0)
                {
                    MessageBox.Show("El paciente ha sido actualizado.", "Actualización de paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //nuevo();

                    if (accionAlGuardarPrestamo == accion.CerrarVentana)
                    {
                        this.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void buscar()
        {
            try
            {
                //Consulta - Consulta de pacientes
                F0302 form = new F0302();
                form.btModificar.Visible = false;
                form.ShowDialog();
                if (F0302.idPaciente != 0)
                {
                    idPaciente = F0302.idPaciente;
                    obtenerPaciente(F0302.idPaciente);
               llCoincidencias.Visible = false;
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void mostrarCoincidencias()
        {
            try
            {
                //Consulta - Consulta de pacientes
                F0302 form = new F0302();
                form.btModificar.Visible = false;
                form.tbNombre.Text = tbNombre.Text;
                form.obtenerPacientes();
                form.ShowDialog();
                idPaciente = F0302.idPaciente;
                obtenerPaciente(F0302.idPaciente);
                if (F0302.idPaciente != 0) llCoincidencias.Visible = false;

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void eliminar()
        {
            try
            {

                if (MessageBox.Show("¿Desea eliminar el paciente?", "Eliminar paciente", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                Paciente paciente = new Paciente();
                paciente.idPaciente = idPaciente;

                int res = PacienteBR.eliminar(paciente);

                if (res != 0)
                {
                    MessageBox.Show("El paciente ha sido eliminado.", "Eliminación de paciente", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    if (accionAlGuardarPrestamo == accion.CerrarVentana)
                    {
                        this.Close();
                    }
                }

                nuevo();



            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void tbClave_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbApellidoPaterno_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbApellidoMaterno_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mtbTelefonoLada_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mtbTelefono_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbTipoPaciente_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpFechaNacimiento_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rbHombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbClave_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
