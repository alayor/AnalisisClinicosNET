﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;


namespace AnalisisClinicos
{
    public partial class F0301 : Form
    {

        BindingList<Resultado_consulta> lsResultados;
        static internal int idResultado;

        public F0301()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpFechaInicio_ValueChanged(object sender, EventArgs e)
        {
            try
            {
               obtenerResultados();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btModificar_Click(object sender, EventArgs e)
        {
            try
            {
                modificar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void modificar()
        {
            try
            {
                if (dgvResultado.Rows.Count > 0 && dgvResultado.SelectedRows[0].Index >= 0)
                {
                    idResultado = (int)lsResultados[dgvResultado.SelectedRows[0].Index].idResultado;

                    F0201 form = new F0201(idResultado);
                    form.accionAlGuardarPrestamo = F0201.accion.NoCerrarVentana;
                    form.btBuscarPaciente.Enabled = false;
                    form.bNuevo.Enabled = false;
                    form.ShowDialog();
                    //obtenerResultados();
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void eliminar()
        {
            try
            {

                ResultadoBR.eliminar((int)lsResultados[dgvResultado.SelectedRows[0].Index].idResultado);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void bCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                idResultado = 0;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvResultado.Rows.Count > 0 && dgvResultado.CurrentCell.RowIndex >= 0)
                {

                    idResultado = lsResultados[dgvResultado.CurrentRow.Index].idResultado;

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0301_Load(object sender, EventArgs e)
        {
            try
            {
                dtpFechaInicio.Value = dtpFechaInicio.Value.AddDays(-7);
                dgvResultado.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular);
                tbClavePaciente.Select();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void obtenerResultados()
        {
            try
            {

                Resultado_consulta resultado = new Resultado_consulta();
                resultado.fechaInicio = dtpFechaInicio.Value;
                resultado.fechaFin = dtpFechaFin.Value;
                resultado.clavePaciente = tbClavePaciente.Text;

                    lsResultados = ResultadoBR.consultarPorFechaClavePaciente(resultado);

                    FDataGridView.Llenar(dgvResultado, lsResultados, "clavePaciente",
                    "nombrePaciente", "fechaModificacion", "fechaCreacion");

                    dgvResultado.Columns["clavePaciente"].Width = 80;
                    dgvResultado.Columns["clavePaciente"].DisplayIndex = 0;
                    dgvResultado.Columns["clavePaciente"].HeaderText = "Clave";

                    dgvResultado.Columns["nombrePaciente"].Width = 200;
                    dgvResultado.Columns["nombrePaciente"].DisplayIndex = 1;
                    dgvResultado.Columns["nombrePaciente"].HeaderText = "Paciente";

                    dgvResultado.Columns["fechaModificacion"].Width = 150;
                    dgvResultado.Columns["fechaModificacion"].DisplayIndex = 2;
                    dgvResultado.Columns["fechaModificacion"].HeaderText = "Modificacion";

                    dgvResultado.Columns["fechaCreacion"].Width = 250;
                    dgvResultado.Columns["fechaCreacion"].DisplayIndex = 3;
                    dgvResultado.Columns["fechaCreacion"].HeaderText = "Creacion";


            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void dtpFechaFin_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                obtenerResultados();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                imprimir();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void imprimir()
        {
            try
            {
                BindingList<ResultadoDetalle_consulta> lsResultadoDetalle_consulta = new BindingList<ResultadoDetalle_consulta>();

                lsResultadoDetalle_consulta = ResultadoDetalleBR.consultar(new ResultadoDetalle_consulta() { idResultado = (int)lsResultados[dgvResultado.CurrentCell.RowIndex].idResultado });

                Reporte<ResultadoDetalle_consulta> reporte = new Reporte<ResultadoDetalle_consulta>(lsResultadoDetalle_consulta, @"rpt/CR0201.rpt");

                Dictionary<String, String> parametros = new Dictionary<string, string>();

                Resultado resultado = new Resultado();
                resultado.idResultado = (int)lsResultados[dgvResultado.CurrentCell.RowIndex].idResultado;

                ResultadoBR.existe(resultado);


                Paciente paciente = new Paciente();
                paciente.idPaciente = resultado.idPaciente;

                PacienteBR.existe(paciente);

                if (paciente.clave == null) paciente.clave = "";

                parametros.Add("CLAVE", paciente.clave);
                parametros.Add("ConFirma", chbFirma.Checked.ToString());
                reporte.Imprimir(parametros);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void tbClave_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    obtenerResultados();

                }
                tbClavePaciente.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                    obtenerResultados();

                tbClavePaciente.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea eliminar el resultado?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    eliminar();
                    MessageBox.Show("El resultado ha sido elimnado", "Eliminar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    obtenerResultados();
                }
              
                
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
