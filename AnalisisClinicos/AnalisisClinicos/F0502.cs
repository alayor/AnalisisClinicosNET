﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using System.IO;

namespace AnalisisClinicos
{
    public partial class F0502 : Form
    {
        public F0502()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Respaldar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Respaldar()
        {

            saveFileDialog1.FileName = "RespaldoAC" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".sql";
            String ruta = saveFileDialog1.FileName;
            DialogResult result = saveFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {

                try
                {
                    String cadenaConexion = readConnectionString();
                    String[] arregloTokens = cadenaConexion.Split(';');
                    Dictionary<String, String> tokens = new Dictionary<String, String>();

                    foreach (String t in arregloTokens)
                    {
                        tokens.Add(t.Substring(0, t.IndexOf("=")).ToLower(), t.Substring(t.IndexOf("=") + 1).ToLower());
                    }


                    process1.StartInfo.FileName = "respaldo.bat";
                    process1.StartInfo.Arguments = tokens["server"] + " " + tokens["port"] + " " + tokens["user"] + " " + tokens["password"] + " " + tokens["database"] + " " + saveFileDialog1.FileName;
                    process1.Start();

                    //System.Diagnostics.Process.Start("cmd.exe", "/C \"C:\\Program Files\\MySQL\\MySQL Server 5.5\\bin\\mysqldump\"" +
                    // " -R -h " + tokens["server"] + " --port=" + tokens["port"] + " -u " + tokens["user"] + " --password=" + tokens["password"] + " --database " + tokens["database"] + " > " + saveFileDialog1.FileName);

                    MessageBox.Show("El respaldo se ha almacenado satisfactoriamente", "Respaldo exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), "Respaldo de información");
                }
            }
        }

        private String readConnectionString()
        {

            StreamReader sr = new StreamReader("./AC.ini");

            try
            {
                return sr.ReadLine();
            }
            finally
            {
                sr.Close();
            }
        }
    }
}
