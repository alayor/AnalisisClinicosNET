﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;


namespace AnalisisClinicos
{
    public partial class F0102 : Form
    {
        BindingList<Grupo> lsGrupo;

        public F0102()
        {
            try
            {
                InitializeComponent();
                llenarCombo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0102_Load(object sender, EventArgs e)
        {
            try
            {
                dgvGrupo.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular);
   
                nuevo();
                dgvGrupo.DataSource = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvGrupos_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGrupo.CurrentRow.Index >= 0)
                {

                    tbNumero.Text = lsGrupo[dgvGrupo.CurrentRow.Index].idGrupo.ToString();
                    tbNombre.Text = lsGrupo[dgvGrupo.CurrentRow.Index].nombre;

                    bEliminar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCategoria_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNumero_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNumero.Text == "0" || tbNumero.Text == "")
                    bGuardar.Text = "&Guardar";
                else
                    bGuardar.Text = "&Actualizar";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCategoria_SelectedValueChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    listar();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void bEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                eliminar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Int32.Parse(tbNumero.Text) == 0)
                {
                    guardar();
                }
                else
                {
                    actualizar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void nuevo()
        {
            try
            {

                tbNumero.Text = "0";
                tbNombre.ResetText();
                dgvGrupo.ClearSelection();
                cbCategoria.Select();

                bEliminar.Enabled = false;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }

        }

        private void llenarCombo()
        {
             try
            {
                 cbCategoria.DisplayMember="nombre";
                 cbCategoria.ValueMember = "idCategoria";
                 cbCategoria.DataSource = CategoriaBR.listar();
                 cbCategoria.SelectedItem = null;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void listar()
        {
            try
            {
                if (cbCategoria.SelectedValue == null)
                    return;


                lsGrupo = GrupoBR.listarPorIdCategoria(new Grupo() { idCategoria = Int32.Parse(cbCategoria.SelectedValue.ToString()) });
                FDataGridView.Llenar(dgvGrupo, lsGrupo, "idGrupo", "nombre");
                
                dgvGrupo.Columns["idGrupo"].Width = 50;
                dgvGrupo.Columns["idGrupo"].HeaderText = "No.";
                dgvGrupo.Columns["nombre"].HeaderText = "Grupo";
                
                dgvGrupo.ClearSelection();
                tbNombre.ResetText();
                cbCategoria.Select();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void guardar()
        {
            try
            {
                if (cbCategoria.SelectedValue == null)
                {
                    MessageBox.Show("Seleccione la categoría del grupo.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbCategoria.Select();
                    return;
                }
                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre del grupo.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }
             

                Grupo grupo = new Grupo();
                grupo.idGrupo = Int32.Parse(tbNumero.Text);
                grupo.idCategoria = Int32.Parse(cbCategoria.SelectedValue.ToString());
                grupo.nombre = tbNombre.Text;

                GrupoBR.insertar(grupo);

                if (grupo.idGrupo != 0)
                {
                    MessageBox.Show("El grupo ha sido registrado.", "Registro de grupo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();
                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void actualizar()
        {
            try
            {

                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre del grupo", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }

                Grupo grupo = new Grupo();

                grupo.idGrupo = Int32.Parse(tbNumero.Text);
                grupo.idCategoria = Int32.Parse(cbCategoria.SelectedValue.ToString());
                grupo.nombre = tbNombre.Text;


                int res = GrupoBR.actualizar(grupo);

                if (res != 0)
                {
                    MessageBox.Show("El grupo ha sido actualizado.", "Actualización de grupo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();
                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void eliminar()
        {
            try
            {

                if (MessageBox.Show("¿Desea eliminar el grupo?", "Eliminar grupo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                Grupo grupo = new Grupo();
                Analisis analisis = new Analisis();

                analisis.idGrupo = Int32.Parse(tbNumero.Text);
                grupo.idGrupo = Int32.Parse(tbNumero.Text);

                BindingList<Analisis> analisisEnGrupo = AnalisisBR.listarPorIdGrupo(analisis);

                if (analisisEnGrupo.Count > 0)
                {
                    String analisis_s = "  Nombre\n";
                    int cont = 0;
                    foreach (Analisis ana in analisisEnGrupo)
                    {
                        analisis_s += "  " + ana.nombre.ToString() + "\n";
                        cont++;

                        if (cont > 7)
                        {
                            analisis_s += "Y la lista continua...";
                            break;
                        }

                    }


                    MessageBox.Show("El grupo no puede ser eliminado ya que esta asignado a " + analisisEnGrupo.Count + " analisis " +
                    "con los siguientes datos: \n" + analisis_s,
                        "Error al eliminar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                int res = GrupoBR.eliminar(grupo);

                if (res != 0)
                {
                    MessageBox.Show("El grupo ha sido eliminado.", "Eliminación de grupo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                listar();
                nuevo();
                }


            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void cbCategoria_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                listar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCategoria_KeyUp_1(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



     
    }
}
