﻿namespace AnalisisClinicos
{
    partial class F0201
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F0201));
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbUnidades = new System.Windows.Forms.TextBox();
            this.cbGrupo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvResultados = new System.Windows.Forms.DataGridView();
            this.btBuscarAnalisis = new System.Windows.Forms.Button();
            this.bNuevo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.bCerrar = new System.Windows.Forms.Button();
            this.bGuardar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNumero = new System.Windows.Forms.TextBox();
            this.tbNombrePaciente = new System.Windows.Forms.TextBox();
            this.btBuscarPaciente = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.lbFechaCreacion = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bImprimir = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lbTotal = new System.Windows.Forms.Label();
            this.tbResultado = new System.Windows.Forms.RichTextBox();
            this.rtbValorReferencia = new System.Windows.Forms.RichTextBox();
            this.chbFirma = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbClavePaciente = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultados)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 20);
            this.label6.TabIndex = 78;
            this.label6.Text = "Valor de referencia";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 20);
            this.label5.TabIndex = 77;
            this.label5.Text = "Unidades";
            // 
            // tbUnidades
            // 
            this.tbUnidades.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUnidades.Location = new System.Drawing.Point(122, 175);
            this.tbUnidades.Name = "tbUnidades";
            this.tbUnidades.ReadOnly = true;
            this.tbUnidades.Size = new System.Drawing.Size(198, 26);
            this.tbUnidades.TabIndex = 70;
            this.tbUnidades.TabStop = false;
            // 
            // cbGrupo
            // 
            this.cbGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGrupo.FormattingEnabled = true;
            this.cbGrupo.Location = new System.Drawing.Point(122, 137);
            this.cbGrupo.Name = "cbGrupo";
            this.cbGrupo.Size = new System.Drawing.Size(369, 28);
            this.cbGrupo.TabIndex = 2;
            this.cbGrupo.SelectionChangeCommitted += new System.EventHandler(this.cbGrupo_SelectionChangeCommitted);
            this.cbGrupo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cbGrupo_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 20);
            this.label4.TabIndex = 76;
            this.label4.Text = "Grupo";
            // 
            // cbCategoria
            // 
            this.cbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Location = new System.Drawing.Point(122, 105);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(369, 28);
            this.cbCategoria.TabIndex = 1;
            this.cbCategoria.SelectedValueChanged += new System.EventHandler(this.cbCategoria_SelectedValueChanged);
            this.cbCategoria.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cbCategoria_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 75;
            this.label1.Text = "Categoria";
            // 
            // dgvResultados
            // 
            this.dgvResultados.AllowUserToAddRows = false;
            this.dgvResultados.AllowUserToDeleteRows = false;
            this.dgvResultados.AllowUserToOrderColumns = true;
            this.dgvResultados.AllowUserToResizeRows = false;
            this.dgvResultados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvResultados.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvResultados.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgvResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResultados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvResultados.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvResultados.Location = new System.Drawing.Point(13, 279);
            this.dgvResultados.MultiSelect = false;
            this.dgvResultados.Name = "dgvResultados";
            this.dgvResultados.RowHeadersVisible = false;
            this.dgvResultados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvResultados.Size = new System.Drawing.Size(675, 205);
            this.dgvResultados.TabIndex = 3;
            this.dgvResultados.TabStop = false;
            this.dgvResultados.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvResultados_CellEnter);
            this.dgvResultados.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvResultados_EditingControlShowing);
            this.dgvResultados.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvResultados_RowEnter);
            this.dgvResultados.SelectionChanged += new System.EventHandler(this.dgvResultados_SelectionChanged);
            this.dgvResultados.Click += new System.EventHandler(this.dgvResultados_Click);
            this.dgvResultados.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dgvResultados_KeyUp);
            // 
            // btBuscarAnalisis
            // 
            this.btBuscarAnalisis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btBuscarAnalisis.Image = ((System.Drawing.Image)(resources.GetObject("btBuscarAnalisis.Image")));
            this.btBuscarAnalisis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBuscarAnalisis.Location = new System.Drawing.Point(568, 147);
            this.btBuscarAnalisis.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btBuscarAnalisis.Name = "btBuscarAnalisis";
            this.btBuscarAnalisis.Size = new System.Drawing.Size(119, 51);
            this.btBuscarAnalisis.TabIndex = 3;
            this.btBuscarAnalisis.TabStop = false;
            this.btBuscarAnalisis.Text = "Buscar\r\n&Resultado";
            this.btBuscarAnalisis.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBuscarAnalisis.UseVisualStyleBackColor = true;
            this.btBuscarAnalisis.Click += new System.EventHandler(this.btBuscarAnalisis_Click);
            // 
            // bNuevo
            // 
            this.bNuevo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNuevo.Image = ((System.Drawing.Image)(resources.GetObject("bNuevo.Image")));
            this.bNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bNuevo.Location = new System.Drawing.Point(578, 87);
            this.bNuevo.Name = "bNuevo";
            this.bNuevo.Size = new System.Drawing.Size(110, 41);
            this.bNuevo.TabIndex = 9;
            this.bNuevo.TabStop = false;
            this.bNuevo.Text = "&Nuevo";
            this.bNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bNuevo.UseVisualStyleBackColor = true;
            this.bNuevo.Click += new System.EventHandler(this.bNuevo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 493);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 82;
            this.label2.Text = "Resultado";
            // 
            // bCerrar
            // 
            this.bCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCerrar.Image = ((System.Drawing.Image)(resources.GetObject("bCerrar.Image")));
            this.bCerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bCerrar.Location = new System.Drawing.Point(568, 555);
            this.bCerrar.Name = "bCerrar";
            this.bCerrar.Size = new System.Drawing.Size(120, 51);
            this.bCerrar.TabIndex = 6;
            this.bCerrar.Text = "&Cerrar";
            this.bCerrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bCerrar.Click += new System.EventHandler(this.bCerrar_Click);
            // 
            // bGuardar
            // 
            this.bGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bGuardar.Image = ((System.Drawing.Image)(resources.GetObject("bGuardar.Image")));
            this.bGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bGuardar.Location = new System.Drawing.Point(399, 555);
            this.bGuardar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bGuardar.Name = "bGuardar";
            this.bGuardar.Size = new System.Drawing.Size(156, 51);
            this.bGuardar.TabIndex = 5;
            this.bGuardar.Text = "&Guardar";
            this.bGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bGuardar.UseVisualStyleBackColor = true;
            this.bGuardar.Click += new System.EventHandler(this.bGuardar_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(573, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 16);
            this.label7.TabIndex = 90;
            this.label7.Text = "No.";
            // 
            // tbNumero
            // 
            this.tbNumero.Enabled = false;
            this.tbNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNumero.Location = new System.Drawing.Point(608, 8);
            this.tbNumero.Name = "tbNumero";
            this.tbNumero.Size = new System.Drawing.Size(79, 22);
            this.tbNumero.TabIndex = 89;
            this.tbNumero.TabStop = false;
            this.tbNumero.TextChanged += new System.EventHandler(this.tbNumero_TextChanged);
            // 
            // tbNombrePaciente
            // 
            this.tbNombrePaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombrePaciente.Location = new System.Drawing.Point(122, 63);
            this.tbNombrePaciente.Name = "tbNombrePaciente";
            this.tbNombrePaciente.ReadOnly = true;
            this.tbNombrePaciente.Size = new System.Drawing.Size(369, 26);
            this.tbNombrePaciente.TabIndex = 1;
            this.tbNombrePaciente.TabStop = false;
            this.tbNombrePaciente.TextChanged += new System.EventHandler(this.tbNombrePaciente_TextChanged);
            // 
            // btBuscarPaciente
            // 
            this.btBuscarPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btBuscarPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btBuscarPaciente.Image")));
            this.btBuscarPaciente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBuscarPaciente.Location = new System.Drawing.Point(23, 14);
            this.btBuscarPaciente.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btBuscarPaciente.Name = "btBuscarPaciente";
            this.btBuscarPaciente.Size = new System.Drawing.Size(173, 41);
            this.btBuscarPaciente.TabIndex = 0;
            this.btBuscarPaciente.Text = "Buscar &Paciente";
            this.btBuscarPaciente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBuscarPaciente.UseVisualStyleBackColor = true;
            this.btBuscarPaciente.Click += new System.EventHandler(this.btConsultarPaciente_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 20);
            this.label8.TabIndex = 92;
            this.label8.Text = "Paciente";
            // 
            // lbFechaCreacion
            // 
            this.lbFechaCreacion.AutoSize = true;
            this.lbFechaCreacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaCreacion.Location = new System.Drawing.Point(13, 18);
            this.lbFechaCreacion.Name = "lbFechaCreacion";
            this.lbFechaCreacion.Size = new System.Drawing.Size(0, 15);
            this.lbFechaCreacion.TabIndex = 93;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbFechaCreacion);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(553, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(135, 38);
            this.groupBox2.TabIndex = 96;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fecha";
            // 
            // bImprimir
            // 
            this.bImprimir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bImprimir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bImprimir.Enabled = false;
            this.bImprimir.Image = ((System.Drawing.Image)(resources.GetObject("bImprimir.Image")));
            this.bImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bImprimir.Location = new System.Drawing.Point(238, 555);
            this.bImprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bImprimir.Name = "bImprimir";
            this.bImprimir.Size = new System.Drawing.Size(147, 51);
            this.bImprimir.TabIndex = 4;
            this.bImprimir.Text = "&Imprimir...";
            this.bImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bImprimir.UseVisualStyleBackColor = true;
            this.bImprimir.Click += new System.EventHandler(this.bImprimir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.lbTotal);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(534, 235);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(153, 38);
            this.groupBox1.TabIndex = 98;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Total";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(90, 19);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(57, 13);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Ver detalle";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // lbTotal
            // 
            this.lbTotal.AutoSize = true;
            this.lbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotal.Location = new System.Drawing.Point(13, 17);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(0, 15);
            this.lbTotal.TabIndex = 93;
            // 
            // tbResultado
            // 
            this.tbResultado.Location = new System.Drawing.Point(122, 490);
            this.tbResultado.Name = "tbResultado";
            this.tbResultado.Size = new System.Drawing.Size(565, 57);
            this.tbResultado.TabIndex = 99;
            this.tbResultado.Text = "";
            this.tbResultado.TextChanged += new System.EventHandler(this.tbResultado_TextChanged);
            // 
            // rtbValorReferencia
            // 
            this.rtbValorReferencia.Location = new System.Drawing.Point(168, 208);
            this.rtbValorReferencia.Name = "rtbValorReferencia";
            this.rtbValorReferencia.ReadOnly = true;
            this.rtbValorReferencia.Size = new System.Drawing.Size(323, 65);
            this.rtbValorReferencia.TabIndex = 0;
            this.rtbValorReferencia.Text = "";
            // 
            // chbFirma
            // 
            this.chbFirma.AutoSize = true;
            this.chbFirma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbFirma.Location = new System.Drawing.Point(148, 589);
            this.chbFirma.Name = "chbFirma";
            this.chbFirma.Size = new System.Drawing.Size(79, 17);
            this.chbFirma.TabIndex = 100;
            this.chbFirma.Text = "Incluir firma";
            this.chbFirma.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(272, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 102;
            this.label3.Text = "Clave";
            // 
            // tbClavePaciente
            // 
            this.tbClavePaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbClavePaciente.Location = new System.Drawing.Point(338, 29);
            this.tbClavePaciente.Name = "tbClavePaciente";
            this.tbClavePaciente.ReadOnly = true;
            this.tbClavePaciente.Size = new System.Drawing.Size(153, 26);
            this.tbClavePaciente.TabIndex = 101;
            this.tbClavePaciente.TabStop = false;
            // 
            // F0201
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 610);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbClavePaciente);
            this.Controls.Add(this.rtbValorReferencia);
            this.Controls.Add(this.tbResultado);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bImprimir);
            this.Controls.Add(this.btBuscarPaciente);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbNombrePaciente);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbNumero);
            this.Controls.Add(this.bCerrar);
            this.Controls.Add(this.bGuardar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bNuevo);
            this.Controls.Add(this.btBuscarAnalisis);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbUnidades);
            this.Controls.Add(this.cbGrupo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbCategoria);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvResultados);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.chbFirma);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "F0201";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resultado";
            this.Load += new System.EventHandler(this.F0201_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultados)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbUnidades;
        private System.Windows.Forms.ComboBox cbGrupo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvResultados;
        internal System.Windows.Forms.Button btBuscarAnalisis;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Button bCerrar;
        private System.Windows.Forms.Button bGuardar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbNumero;
        private System.Windows.Forms.TextBox tbNombrePaciente;
        internal System.Windows.Forms.Button btBuscarPaciente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbFechaCreacion;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Button bNuevo;
        private System.Windows.Forms.Button bImprimir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbTotal;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.RichTextBox tbResultado;
        private System.Windows.Forms.RichTextBox rtbValorReferencia;
        private System.Windows.Forms.CheckBox chbFirma;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbClavePaciente;

    }
}