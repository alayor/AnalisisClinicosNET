﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;


namespace AnalisisClinicos
{
    public partial class F0201 : Form
    {
        BindingList<ResultadoDetalle> lsResultadoDetalle = new BindingList<ResultadoDetalle>();
        BindingList<ResultadoDetalle> lsResultadoDetalleAlmacenado = new BindingList<ResultadoDetalle>();
        int idPaciente = 0;
        Resultado resultado = new Resultado();
        Boolean mostrarMensaje = true;
        internal accion accionAlGuardarPrestamo = accion.NoCerrarVentana;


        internal enum accion
        {
            NoCerrarVentana,
            CerrarVentana
        }

        public F0201(int idResultado)
            : this()
        {
            try
            {
                obtenerResultado(idResultado);
                dgvResultados.DataSource = null;
                dgvResultados.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void obtenerResultado(int idResultado)
        {
            try
            {

                resultado.idResultado = idResultado;
                if (ResultadoBR.existe(resultado))
                {

                    tbNumero.Text = resultado.idResultado.ToString();
                    tbNombrePaciente.Text = resultado.nombrePaciente;
                    idPaciente = resultado.idPaciente;
                    lbFechaCreacion.Text = resultado.fechaCreacion.ToShortDateString();
                    tbClavePaciente.Text = resultado.clavePaciente;

                    lsResultadoDetalleAlmacenado = ResultadoDetalleBR.listarPorIdResultado(new ResultadoDetalle() { idResultado = resultado.idResultado });
                    ActualizarPrecio();
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        public F0201()
        {
            try
            {

                InitializeComponent();
              
                llenarComboCategoria();
                cbGrupo.DataSource = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0201_Load(object sender, EventArgs e)
        {
            try
            {
                dgvResultados.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular);



                btBuscarPaciente.Select();


                dgvResultados.ClearSelection();
                tbResultado.ResetText();

                this.dgvResultados.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(dgvResultados_EditingControlShowing);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea limpiar todos los campos?", "Nuevo resultado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    nuevo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvResultados_Click(object sender, EventArgs e)
        {
            try
            {

                //  obtenerAnalisisCompletos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCategoria_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    llenarComboGrupo();
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCategoria_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                llenarComboGrupo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbGrupo_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                     //System.Windows.Forms.SendKeys.Send("{TAB}");

                    dgvResultados.Select();
                    System.Windows.Forms.SendKeys.Send("{RIGHT}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbNumero.Text == "")
                {
                    guardar();
                }
                else
                {
                    actualizar();
                }

                if (accionAlGuardarPrestamo == accion.CerrarVentana)
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (lsResultadoDetalle.Count > 0)
                {
                    if (MessageBox.Show("¿Desea cerrar la ventana?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        this.Close();
                    }
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btConsultarPaciente_Click(object sender, EventArgs e)
        {
            //Consulta - Paciente
            try
            {
                F0302 form = new F0302();
                form.btModificar.Visible = false;
                form.ShowDialog();
                idPaciente = F0302.idPaciente;
                obtenerPaciente(idPaciente);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvResultados_SelectionChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (lsResultadoDetalle.Count > 0)
            //    {
            //        tbResultado.Text = lsResultadoDetalle[dgvResultados.CurrentRow.Index].resultado;
            //        obtenerAnalisis(lsResultadoDetalle[dgvResultados.CurrentRow.Index].idAnalisis);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void btBuscarAnalisis_Click(object sender, EventArgs e)
        {
            try
            {
                buscarAnalisis();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Actualizar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btQuitar_Click(object sender, EventArgs e)
        {

            try
            {
                Quitar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNumero_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNumero.Text == "0" || tbNumero.Text == "")
                    bGuardar.Text = "&Guardar";
                else
                    bGuardar.Text = "&Actualizar";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                mostrarMensaje = false;
                bGuardar_Click(null, null);
                imprimir();
                mostrarMensaje = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbGrupo_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void tbNombrePaciente_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(tbNombrePaciente.Text))
                {
                    bGuardar.Enabled = false;
                    bImprimir.Enabled = false;
                }
                else
                {
                    bGuardar.Enabled = true;
                    bImprimir.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                listarPrecios();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }





        private void obtenerPaciente(int idPaciente)
        {
            try
            {
                Paciente paciente_existe = new Paciente();
                paciente_existe.idPaciente = idPaciente;
                if (PacienteBR.existe(paciente_existe))
                {
                    tbNombrePaciente.Text = paciente_existe.nombre + " " + paciente_existe.apellidoPaterno + " " + paciente_existe.apellidoMaterno;
                    tbClavePaciente.Text = paciente_existe.clave;
                    cbCategoria.Select();
                }
                else
                {
                    btBuscarPaciente.Select();
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void Agregar()
        {
            try
            {
                if (dgvResultados.CurrentRow != null)
                {
                    if (lsResultadoDetalleAlmacenado.Contains(lsResultadoDetalle[dgvResultados.CurrentRow.Index]))
                    {
                        
                        (from trade in lsResultadoDetalleAlmacenado
                         where trade.idAnalisis == lsResultadoDetalle[dgvResultados.SelectedRows[0].Index].idAnalisis
                         select trade).ToList().ForEach(trade => trade.resultado = lsResultadoDetalle[dgvResultados.SelectedRows[0].Index].resultado);

                      
                    }
                    else
                    {
                        lsResultadoDetalleAlmacenado.Add(lsResultadoDetalle[dgvResultados.SelectedRows[0].Index]);
                    }

                    ActualizarPrecio();
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void ActualizarPrecio()
        {
            try
            {
                decimal total = 0;




                foreach (ResultadoDetalle resultado in limpiarResultadoDetalleAlmacenado(lsResultadoDetalleAlmacenado))
                {
                    Paciente paciente = new Paciente();
                    paciente.idPaciente = idPaciente;
                    PacienteBR.existe(paciente);

                    PrecioAnalisis precioAnalisis = new PrecioAnalisis();
                    precioAnalisis.idAnalisis = resultado.idAnalisis;
                    precioAnalisis.idTipoPaciente = paciente.idTipoPaciente;

                    if (PrecioAnalisisBR.existePorAnalisisTipoPaciente(precioAnalisis))
                    {
                        total += precioAnalisis.precio;
                    }
                    else
                    {
                        total += 0;
                    }
                }

                lbTotal.Text = "$ " + (total * IVA() + total).ToString("0.00");
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private BindingList<ResultadoDetalle> limpiarResultadoDetalleAlmacenado(BindingList<ResultadoDetalle> lista)
        {
            BindingList<ResultadoDetalle> resultado = new BindingList<ResultadoDetalle>();
            foreach (ResultadoDetalle res in lista)
            {
                if (res.resultado != "" && res.resultado != null)
                {
                    resultado.Add(res);
                }
            }
            return resultado;
        }


        private void obtenerAnalisis(int idAnalisis)
        {
            try
            {
                Analisis analisis = new Analisis();
                analisis.idAnalisis = idAnalisis;


                if (AnalisisBR.existe(analisis))
                {

                    Grupo grupo = new Grupo();

                    grupo.idGrupo = analisis.idGrupo;

                    if (GrupoBR.existe(grupo))
                    {
                        cbCategoria.SelectedValue = grupo.idCategoria;
                    }

                    cbGrupo.SelectedValue = analisis.idGrupo;
                    listarAnalisis();
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void obtenerValorUnidades(int idAnalisis)
        {
            try
            {
                Analisis analisis = new Analisis();
                analisis.idAnalisis = idAnalisis;


                if (AnalisisBR.existe(analisis))
                {


                    tbUnidades.Text = analisis.unidades;
                    rtbValorReferencia.Text = analisis.valorReferencia;


                }




            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void ActualizarGrid()
        {
            FDataGridView.Llenar(dgvResultados, lsResultadoDetalleAlmacenado, "nombreAnalisis", "resultado");

            dgvResultados.Columns["nombreAnalisis"].HeaderText = "Analisis";
            dgvResultados.Columns["resultado"].HeaderText = "Resultado";
        }

        private void nuevo()
        {
            try
            {

                tbNumero.Text = "";
                tbNombrePaciente.ResetText();

                tbResultado.ResetText();
                tbUnidades.ResetText();
                rtbValorReferencia.ResetText();
                lsResultadoDetalle.Clear();
                lsResultadoDetalleAlmacenado.Clear();
                //dgvResultados.DataSource = lsResultado;




                llenarComboCategoria();

                lsResultadoDetalle.Clear();
                ActualizarGrid();
                btBuscarPaciente.Select();



            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }

        }

        private void llenarComboCategoria()
        {
            try
            {
                cbCategoria.DisplayMember = "nombre";
                cbCategoria.ValueMember = "idCategoria";
                cbCategoria.DataSource = CategoriaBR.listar();
                cbCategoria.SelectedItem = null;

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void llenarComboGrupo()
        {
            try
            {
                if (cbCategoria.SelectedValue != null)
                {

                    Grupo grupo = new Grupo();
                    grupo.idCategoria = Int32.Parse(cbCategoria.SelectedValue.ToString());

                    cbGrupo.DisplayMember = "nombre";
                    cbGrupo.ValueMember = "idGrupo";
                    cbGrupo.DataSource = GrupoBR.listarPorIdCategoria(grupo);
                    cbGrupo.SelectedItem = null;


                    tbResultado.ResetText();
                    tbUnidades.ResetText();
                    rtbValorReferencia.ResetText();

                    lsResultadoDetalle.Clear();
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }


        private void listarPrecios()
        {
            decimal total = 0;

            BindingList<Precios> listaPrecios = new BindingList<Precios>();

            foreach (ResultadoDetalle resultado in limpiarResultadoDetalleAlmacenado(lsResultadoDetalleAlmacenado))
            {
                Paciente paciente = new Paciente();
                paciente.idPaciente = idPaciente;
                PacienteBR.existe(paciente);

                PrecioAnalisis precioAnalisis = new PrecioAnalisis();
                precioAnalisis.idAnalisis = resultado.idAnalisis;
                precioAnalisis.idTipoPaciente = paciente.idTipoPaciente;

                if (PrecioAnalisisBR.existePorAnalisisTipoPaciente(precioAnalisis))
                {
                    listaPrecios.Add(new Precios(resultado.nombreAnalisis, "$ " + precioAnalisis.precio.ToString()));

                    total += precioAnalisis.precio;
                }
                else
                {
                    listaPrecios.Add(new Precios(resultado.nombreAnalisis, "$ 0.00"));
                    total += 0;
                }
            }

            F0201a form = new F0201a();
            form.lbResultado.Text += "\nSUBTOTAL: \t$ " + total.ToString("0.00");
            form.lbResultado.Text += "\nIVA : \t\t$ " + (total * IVA()).ToString("0.00");
            form.lbResultado.Text += "\nTOTAL: \t\t$ " + (total * IVA() + total).ToString("0.00");
            form.listaPrecios = listaPrecios;


            form.ShowDialog();
        }

        private decimal IVA()
        {
            StreamReader sr = new StreamReader("./IVA.txt");

            try
            {
                return decimal.Parse(sr.ReadLine()) / 100;
            }
            finally
            {
                sr.Close();
            }
        }

        private void guardar()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(tbNombrePaciente.Text))
                {
                    MessageBox.Show("Seleccione el paciente.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombrePaciente.Select();
                    return;
                }




                resultado.idPaciente = idPaciente;
                resultado.nombrePaciente = tbNombrePaciente.Text;
                resultado.fechaCreacion = DateTime.Now;
                resultado.fechaModificacion = resultado.fechaCreacion;
                resultado.clavePaciente = tbClavePaciente.Text;

                ResultadoBR.insertar(resultado);

                //Insertar detalle
                foreach (ResultadoDetalle res_det in limpiarResultadoDetalleAlmacenado(lsResultadoDetalleAlmacenado))
                {
                    res_det.idResultado = resultado.idResultado;

                    ResultadoDetalleBR.insertar(res_det);
                }

                if (resultado.idResultado != 0)
                {
                    if(mostrarMensaje)
                    MessageBox.Show("El resultado ha sido registrado.", "Registro de resultado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    tbNumero.Text = resultado.idResultado.ToString();
                    lbFechaCreacion.Text = resultado.fechaCreacion.ToShortDateString();
                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void actualizar()
        {
            try
            {

                if (String.IsNullOrWhiteSpace(tbNombrePaciente.Text))
                {
                    MessageBox.Show("Ingrese el nombre del paciente", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombrePaciente.Select();
                    return;
                }


                resultado.idResultado = Int32.Parse(tbNumero.Text);
                resultado.idPaciente = idPaciente;
                resultado.nombrePaciente = tbNombrePaciente.Text;
                resultado.fechaModificacion = DateTime.Now;
                resultado.clavePaciente = tbClavePaciente.Text;

                int res = ResultadoBR.actualizar(resultado);


                //Eliminar detalle
                ResultadoDetalle resultadoDetalle = new ResultadoDetalle();
                resultadoDetalle.idResultado = Int32.Parse(tbNumero.Text);
                ResultadoDetalleBR.eliminar(resultadoDetalle);


                //Insertar detalle
                foreach (ResultadoDetalle res_det in limpiarResultadoDetalleAlmacenado(lsResultadoDetalleAlmacenado))
                {
                    res_det.idResultado = resultado.idResultado;

                    ResultadoDetalleBR.insertar(res_det);
                }

                if (res != 0)
                {
                    if (mostrarMensaje)
                    MessageBox.Show("El resultado ha sido actualizado.", "Actualización de resultado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void imprimir()
        {
            try
            {
                BindingList<ResultadoDetalle_consulta> lsResultadoDetalle_consulta = new BindingList<ResultadoDetalle_consulta>();

                lsResultadoDetalle_consulta = ResultadoDetalleBR.consultar(new ResultadoDetalle_consulta() { idResultado = Int32.Parse(tbNumero.Text) });

                Reporte<ResultadoDetalle_consulta> reporte = new Reporte<ResultadoDetalle_consulta>(lsResultadoDetalle_consulta, @"rpt/CR0201.rpt");

                Dictionary<String, String> parametros = new Dictionary<string, string>();

                Paciente paciente = new Paciente();
                paciente.idPaciente = idPaciente;

                PacienteBR.existe(paciente);

                if (paciente.clave ==null) paciente.clave = "";

                parametros.Add("CLAVE", paciente.clave);
                parametros.Add("ConFirma", chbFirma.Checked.ToString());
                //parametros.Add("FechaFin", dtpFechaFin.Value.ToString());

                reporte.Imprimir(parametros);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void Quitar()
        {
            try
            {
                if (dgvResultados.CurrentRow != null)
                {
                    lsResultadoDetalle.RemoveAt(dgvResultados.CurrentRow.Index);

                    ActualizarPrecio();
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void Actualizar()
        {
            try
            {
                if (dgvResultados.CurrentRow != null)
                {
                    lsResultadoDetalle[dgvResultados.CurrentRow.Index].resultado = tbResultado.Text;


                    dgvResultados.Refresh();

                    tbResultado.Text = lsResultadoDetalle[dgvResultados.CurrentRow.Index].resultado;

                    ActualizarPrecio();
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void buscarAnalisis()
        {
            try
            {
                //Consulta - Consulta de pacientes
                F0301 form = new F0301();
                form.btModificar.Visible = false;
                form.ShowDialog();
                obtenerResultado(F0301.idResultado);
                

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void listarAnalisis()
        {
            try
            {
                if (cbGrupo.SelectedValue == null)
                    return;

                lsResultadoDetalle.Clear();


                BindingList<Analisis> lsAnalisis = AnalisisBR.listarPorIdGrupo(new Analisis() { idGrupo = Int32.Parse(cbGrupo.SelectedValue.ToString()) });


                foreach (Analisis analisis in lsAnalisis)
                {
                    ResultadoDetalle resultado = new ResultadoDetalle();
                    resultado.idAnalisis = analisis.idAnalisis;
                    resultado.nombreAnalisis = analisis.nombre;

                    lsResultadoDetalle.Add(resultado);
                }

                for (int i = 0; i < lsResultadoDetalle.Count; i++)
                {
                    for (int j = 0; j < lsResultadoDetalleAlmacenado.Count; j++)
                    {
                        if (lsResultadoDetalleAlmacenado[j].Equals(lsResultadoDetalle[i]))
                        {
                            lsResultadoDetalle[i].resultado = lsResultadoDetalleAlmacenado[j].resultado;
                        }
                    }
                }

                FDataGridView.Llenar(dgvResultados, lsResultadoDetalle, "idAnalisis", "nombreAnalisis", "nombreAnalisis", "resultado");

                dgvResultados.Columns["idAnalisis"].HeaderText = "idAnalisis";
                dgvResultados.Columns["idAnalisis"].Width = 50;
               // dgvResultados.Columns["idAnalisis"].ReadOnly = true;
                dgvResultados.Columns["nombreAnalisis"].HeaderText = "Análisis";
                dgvResultados.Columns["nombreAnalisis"].ReadOnly = true;
                dgvResultados.Columns["resultado"].HeaderText = "Análisis";


                dgvResultados.ClearSelection();
                cbGrupo.Select();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void cbGrupo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                listarAnalisis();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            cbGrupo.Select();
        }

        private void dgvResultados_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (dgvResultados.CurrentRow != null)
                {
                    if (e.KeyData == Keys.Enter && dgvResultados.CurrentRow.Index == dgvResultados.Rows.Count)
                    {
                        System.Windows.Forms.SendKeys.Send("{TAB}");
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        TextBox tb;
        private void dgvResultados_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvResultados.CurrentCell.ColumnIndex == 4)
            {
               tb= (TextBox)e.Control;

     
                tb.TextChanged += new EventHandler(tb_TextChanged);
  
                tb.KeyUp += new KeyEventHandler(tb_KeyUp);

                var source = new AutoCompleteStringCollection();
                String[] stringArray = { "NEGATIVO", "POSITIVO" };
                source.AddRange(stringArray);

                TextBox prodCode = e.Control as TextBox;
                if (prodCode != null)
                {
                    prodCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    prodCode.AutoCompleteCustomSource = source;
                    prodCode.AutoCompleteSource = AutoCompleteSource.CustomSource;

                }
            }
            e.CellStyle.BackColor = Color.White;
            dgvResultados.CurrentCell.Style.BackColor = Color.White;




        }

        void tb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvResultados.IsCurrentCellInEditMode && dgvResultados.SelectedRows.Count>0)
                {

                    tbResultado.Text = dgvResultados.CurrentCell.EditedFormattedValue.ToString();

                    lsResultadoDetalle[dgvResultados.SelectedRows[0].Index].resultado = tbResultado.Text;

                    
                    
                    Agregar();

                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        void tb_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (dgvResultados.SelectedRows.Count>0)
                {
                    if (e.KeyValue == 13 && dgvResultados.SelectedRows[0].Index == dgvResultados.Rows.Count)
                    {
                        System.Windows.Forms.SendKeys.Send("{TAB}");
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbResultado_TextChanged(object sender, EventArgs e)
        {

            try
            {
                if (dgvResultados.SelectedRows != null && dgvResultados.CurrentRow != null)
                    if (tbResultado.Focused && dgvResultados.SelectedRows.Count>0)
                {

                    lsResultadoDetalle[dgvResultados.SelectedRows[0].Index].resultado = tbResultado.Text;

                    Agregar();




                    dgvResultados.Refresh();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void dgvResultados_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (lsResultadoDetalle.Count > 0 && dgvResultados.CurrentRow != null && dgvResultados.SelectedRows.Count>0)
                {
                    if (dgvResultados.SelectedRows.Count > 0)
                    {
                        tbResultado.Text = lsResultadoDetalle[dgvResultados.SelectedRows[0].Index].resultado;
                    }
                    else
                    {
                        tbResultado.Text = lsResultadoDetalle[dgvResultados.SelectedRows[0].Index].resultado;
                    }
                 
                    obtenerValorUnidades(lsResultadoDetalle[dgvResultados.SelectedRows[0].Index].idAnalisis);

                    if (tb != null)
                    {
                        tb.TextChanged -= new EventHandler(tb_TextChanged);
                        tb.KeyUp -= new KeyEventHandler(tb_KeyUp);
                        tb.TextChanged -= new EventHandler(tb_TextChanged);
                        tb.KeyUp -= new KeyEventHandler(tb_KeyUp);
                    }
                 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvResultados_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgvResultados.CurrentCell.ReadOnly == true && dgvResultados.Focused)
            //{
            //    System.Windows.Forms.SendKeys.Send("{RIGHT}");
            //}
        }





    }
}
