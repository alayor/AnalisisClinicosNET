﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;


namespace AnalisisClinicos
{
    public partial class F0104 : Form
    {

        BindingList<TipoPaciente> lsTipoPaciente;


        public F0104()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0104_Load(object sender, EventArgs e)
        {
            try
            {
                dgvTiposPersona.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular);
                listar();
                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvTiposPersona_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvTiposPersona.CurrentRow.Index >= 0)
                {
                    tbNumero.Text = lsTipoPaciente[dgvTiposPersona.CurrentRow.Index].idTipoPaciente.ToString();
                    tbNombre.Text = lsTipoPaciente[dgvTiposPersona.CurrentRow.Index].nombre;

                    bEliminar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNumero_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNumero.Text == "0" || tbNumero.Text == "")
                    bGuardar.Text = "&Guardar";
                else
                    bGuardar.Text = "&Actualizar";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                eliminar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Int32.Parse(tbNumero.Text) == 0)
                {
                    guardar();
                }
                else
                {
                    actualizar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void nuevo()
        {
            try
            {

                tbNumero.Text = "0";
                tbNombre.ResetText();
                tbNombre.Select();
                dgvTiposPersona.ClearSelection();

                bEliminar.Enabled = false;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }

        }


        private void listar()
        {
            try
            {

                lsTipoPaciente = TipoPacienteBR.listar();
                FDataGridView.Llenar(dgvTiposPersona, lsTipoPaciente, "idTipoPaciente", "nombre");
                dgvTiposPersona.Columns["idTipoPaciente"].HeaderText = "No.";
                dgvTiposPersona.Columns["idTipoPaciente"].Width = 120;
                dgvTiposPersona.Columns["nombre"].HeaderText = "Categoria";


                dgvTiposPersona.ClearSelection();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }


        private void guardar()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre del tipo de paciente.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }

                TipoPaciente paciente = new TipoPaciente();
                paciente.idTipoPaciente = Int32.Parse(tbNumero.Text);
                paciente.nombre = tbNombre.Text;

                TipoPacienteBR.insertar(paciente);

                if (paciente.idTipoPaciente != 0)
                {
                    MessageBox.Show("El tipo de paciente ha sido registrado.", "Registro de tipo de paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();
                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void actualizar()
        {
            try
            {

                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre del tipo de paciente", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }

                TipoPaciente paciente = new TipoPaciente();

                paciente.idTipoPaciente = Int32.Parse(tbNumero.Text);
                paciente.nombre = tbNombre.Text;


                int res = TipoPacienteBR.actualizar(paciente);

                if (res != 0)
                {
                    MessageBox.Show("El tipo de paciente ha sido actualizado.", "Actualización de tipo de paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();
                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void eliminar()
        {
            try
            {

                if (MessageBox.Show("¿Desea eliminar el tipo de paciente?", "Eliminar tipo de paciente", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                TipoPaciente tipoPaciente = new TipoPaciente();
                Paciente paciente = new Paciente();

                paciente.idTipoPaciente = Int32.Parse(tbNumero.Text);
                tipoPaciente.idTipoPaciente = Int32.Parse(tbNumero.Text);

                BindingList<Paciente> pacientesEnTipoPaciente = PacienteBR.listarPorIdTipoPaciente(paciente);

                if (pacientesEnTipoPaciente.Count > 0)
                {
                    String pacientes = "  Nombre\n";
                    int cont = 0;
                    foreach (Paciente pac in pacientesEnTipoPaciente)
                    {
                        pacientes += "  " + pac.nombre.ToString() + "\n";
                        cont++;

                        if (cont > 7)
                        {
                            pacientes += "Y la lista continua...";
                            break;
                        }

                    }


                    MessageBox.Show("El tipo de paciente no puede ser eliminado ya que esta asignado a " + pacientesEnTipoPaciente.Count + " pacientes(s) " +
                    "con los siguientes datos: \n" + pacientes,
                        "Error al eliminar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    int res = TipoPacienteBR.eliminar(tipoPaciente);

                    if (res != 0)
                    {
                        MessageBox.Show("El tipo de paciente ha sido eliminado.", "Eliminación de tipo de paciente", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    listar();
                    nuevo();
                }


            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void tbNombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
