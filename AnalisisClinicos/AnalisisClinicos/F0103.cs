﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;

namespace AnalisisClinicos
{
    public partial class F0103 : Form
    {
        BindingList<Analisis> lsAnalisis;

        public F0103()
        {
            try
            {
                InitializeComponent();
                llenarComboCategoria();
                llenarComboTipoPaciente();
                cbGrupo.DataSource = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0103_Load(object sender, EventArgs e)
        {
            try
            {
                dgvAnalisis.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular);

                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message , "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message , "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvAnalisis_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAnalisis.CurrentRow.Index >= 0)
                {

                    tbNumero.Text = lsAnalisis[dgvAnalisis.CurrentRow.Index].idAnalisis.ToString();
                    cbGrupo.SelectedValue = lsAnalisis[dgvAnalisis.CurrentRow.Index].idGrupo;
                    tbNombre.Text = lsAnalisis[dgvAnalisis.CurrentRow.Index].nombre;
                    tbUnidades.Text = lsAnalisis[dgvAnalisis.CurrentRow.Index].unidades;
                    tbValorReferencia.Text = lsAnalisis[dgvAnalisis.CurrentRow.Index].valorReferencia;

                    obtenerPrecio();
                    
                    bEliminar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void obtenerPrecio()
        {
            try
            {

                if (cbTipoPaciente.SelectedValue != null && dgvAnalisis.CurrentRow != null)
                {



                   //Insertar si no existe                   
                        PrecioAnalisis precioAnalisis = new PrecioAnalisis();                       
                        precioAnalisis.idTipoPaciente = Int32.Parse(cbTipoPaciente.SelectedValue.ToString());
                        precioAnalisis.idAnalisis = lsAnalisis[dgvAnalisis.CurrentRow.Index].idAnalisis;
                    if( ! PrecioAnalisisBR.existePorAnalisisTipoPaciente(precioAnalisis))
                    {
                        precioAnalisis.precio = 0;
                        PrecioAnalisisBR.insertar(precioAnalisis);
                    }
                                        
                    
                    precioAnalisis.idAnalisis = lsAnalisis[dgvAnalisis.SelectedRows[0].Index].idAnalisis;
                    precioAnalisis.idTipoPaciente = Int32.Parse(cbTipoPaciente.SelectedValue.ToString());

                    if (PrecioAnalisisBR.existePorAnalisisTipoPaciente(precioAnalisis))
                    {

                        tbPrecioAnalisis.Text = precioAnalisis.precio.ToString();
                    }
                    else
                    {
                        tbPrecioAnalisis.Text = "0";
                    }

                }
                else
                {
                    tbPrecioAnalisis.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void cbCategoria_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    llenarComboGrupo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message , "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNumero_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNumero.Text == "0" || tbNumero.Text == "")
                    bGuardar.Text = "&Guardar";
                else
                    bGuardar.Text = "&Actualizar";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message , "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCategoria_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                llenarComboGrupo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message , "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbGrupo_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    listar();
                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbGrupo_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
               // listar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                eliminar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Int32.Parse(tbNumero.Text) == 0)
                {
                    guardar();
                }
                else
                {
                    actualizar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void nuevo()
        {
            try
            {

                tbNumero.Text = "0";
                tbNombre.ResetText();
                tbUnidades.ResetText();
                tbValorReferencia.ResetText();
                dgvAnalisis.ClearSelection();
                tbPrecioAnalisis.Text = "0";
                cbCategoria.Select();

                bEliminar.Enabled = false;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }

        }

        private void llenarComboCategoria()
        {
            try
            {
                cbCategoria.DisplayMember = "nombre";
                cbCategoria.ValueMember = "idCategoria";
                cbCategoria.DataSource = CategoriaBR.listar();
                cbCategoria.SelectedItem = null;

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void llenarComboGrupo()
        {
            try
            {
                if (cbCategoria.SelectedValue != null)
                {
                   
                    Grupo grupo = new Grupo();
                    grupo.idCategoria = Int32.Parse(cbCategoria.SelectedValue.ToString());

                    cbGrupo.DisplayMember = "nombre";
                    cbGrupo.ValueMember = "idGrupo";
                    cbGrupo.DataSource = GrupoBR.listarPorIdCategoria(grupo);
                    cbGrupo.SelectedItem = null;


                   // dgvAnalisis.DataSource = null;
                    tbNombre.ResetText();
                    tbUnidades.ResetText();
                    tbValorReferencia.ResetText();
                    cbCategoria.Select();

                    listar();
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void llenarComboTipoPaciente()
        {
            try
            {
                cbTipoPaciente.DisplayMember = "nombre";
                cbTipoPaciente.ValueMember = "idTipoPaciente";
                cbTipoPaciente.DataSource = TipoPacienteBR.listar();
                cbTipoPaciente.SelectedItem = null;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void listar()
        {
            try
            {
                if (lsAnalisis == null)
                {
                    lsAnalisis = new BindingList<Analisis>();
                }
                dgvAnalisis.DataSource = null;

                if (cbGrupo.SelectedValue == null )
                {
                    return;
                }

                nuevo();


                lsAnalisis = AnalisisBR.listarPorIdGrupo(new Analisis() { idGrupo = Int32.Parse(cbGrupo.SelectedValue.ToString()) });
                FDataGridView.Llenar(dgvAnalisis, lsAnalisis, "idAnalisis", "nombre", "unidades", "valorReferencia");

              
                    dgvAnalisis.Columns["idAnalisis"].HeaderText = "No.";
                    dgvAnalisis.Columns["idAnalisis"].Width = 50;
                    dgvAnalisis.Columns["nombre"].HeaderText = "Análisis";
                    dgvAnalisis.Columns["unidades"].HeaderText = "Unidades";
                    dgvAnalisis.Columns["valorReferencia"].HeaderText = "Referencia";
             


                dgvAnalisis.ClearSelection();
                cbGrupo.Select();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void guardar()
        {
            try
            {
                if (cbCategoria.SelectedValue == null)
                {
                    MessageBox.Show("Seleccione la categoría del analisis.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbCategoria.Select();
                    return;
                }
                if (cbGrupo.SelectedValue == null)
                {
                    MessageBox.Show("Seleccione el grupo del analisis.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbGrupo.Select();
                    return;
                }
                try { Double.Parse(tbPrecioAnalisis.Text); }
                catch
                {
                    tbPrecioAnalisis.Text = "0";
                }
                //if (String.IsNullOrWhiteSpace(tbNombre.Text))
                //{
                //    MessageBox.Show("Ingrese el nombre del análisis.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    tbNombre.Select();
                //    return;
                //}

                if (cbTipoPaciente.SelectedValue != null)
                {
                    PrecioAnalisis precioAnalisis = new PrecioAnalisis();
                    precioAnalisis.precio = Decimal.Parse(tbPrecioAnalisis.Text);
                    precioAnalisis.idTipoPaciente = Int32.Parse(cbTipoPaciente.SelectedValue.ToString());
                    precioAnalisis.idAnalisis = lsAnalisis[dgvAnalisis.CurrentRow.Index].idAnalisis;

                    int res_pre = PrecioAnalisisBR.actualizarPorAnalisiTipoPaciente(precioAnalisis);

                    if (res_pre == 0)
                    {
                        MessageBox.Show("El precio del analisis no se ha podido actualizar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                }
              

                Analisis analisis = new Analisis();
                analisis.idGrupo = Int32.Parse(cbGrupo.SelectedValue.ToString());
                analisis.nombre = tbNombre.Text;
                analisis.unidades = tbUnidades.Text;
                analisis.valorReferencia = tbValorReferencia.Text;


                AnalisisBR.insertar(analisis);

                if (analisis.idAnalisis != 0)
                {
                    MessageBox.Show("El análisis ha sido registrado.", "Registro de análisis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();

                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void actualizar()
        {
            try
            {

                //if (String.IsNullOrWhiteSpace(tbNombre.Text))
                //{
                //    MessageBox.Show("Ingrese el nombre del análisis", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    tbNombre.Select();
                //    return;
                //}
                if (cbCategoria.SelectedValue == null)
                {
                    MessageBox.Show("Seleccione la categoría del analisis.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbCategoria.Select();
                    return;
                }
                if (cbGrupo.SelectedValue == null)
                {
                    MessageBox.Show("Seleccione el grupo del analisis.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbGrupo.Select();
                    return;
                }
                try { Double.Parse(tbPrecioAnalisis.Text); }
                catch
                {
                    tbPrecioAnalisis.Text = "0";
                }

                if (cbTipoPaciente.SelectedValue != null)
                {
                    PrecioAnalisis precioAnalisis = new PrecioAnalisis();
                    precioAnalisis.precio = Decimal.Parse(tbPrecioAnalisis.Text);
                    precioAnalisis.idTipoPaciente = Int32.Parse(cbTipoPaciente.SelectedValue.ToString());
                    precioAnalisis.idAnalisis = lsAnalisis[dgvAnalisis.CurrentRow.Index].idAnalisis;

                    int res_pre = PrecioAnalisisBR.actualizarPorAnalisiTipoPaciente(precioAnalisis);

                    if (res_pre == 0)
                    {
                        MessageBox.Show("El precio del analisis no se ha podido actualizar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
              
                    }
                }



                Analisis analisis = new Analisis();
                analisis.idAnalisis = Int32.Parse(tbNumero.Text);
                analisis.idGrupo = Int32.Parse(cbGrupo.SelectedValue.ToString());
                analisis.nombre = tbNombre.Text;
                analisis.unidades = tbUnidades.Text;
                analisis.valorReferencia = tbValorReferencia.Text;

                int res = AnalisisBR.actualizar(analisis);

                if (res != 0)
                {
                    MessageBox.Show("El análisis ha sido actualizado.", "Actualización de análisis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();
                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void eliminar()
        {
            try
            {

                //if (MessageBox.Show("¿Desea eliminar la clasificación?", "Eliminar clasificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                //{
                //    return;
                //}

                Analisis analisis = new Analisis();
                //Ingreso ingreso = new Ingreso();

                //ingreso.idConceptoIngreso = idConceptoIngreso;
                analisis.idAnalisis = Int32.Parse(tbNumero.Text);

                //List<Ingreso> ingresosEnConceptoIngreso = IngresoBR.consultar_por_idConceptoIngreso(ingreso);

                //if (ingresosEnConceptoIngreso.Count > 0)
                //{
                //    String ingresos = "  Id\t\tCantidad\t\tFecha\n";
                //    int cont = 0;
                //    foreach (Ingreso ing in ingresosEnConceptoIngreso)
                //    {
                //        ingresos += "  " + ing.idIngreso.ToString() + "\t\t" + ing.cantidad.ToString() + "\t\t" + ing.fecha.ToString() + "\n";
                //        cont++;

                //        if (cont > 7)
                //        {
                //            ingresos += "Y la lista continua...";
                //            break;
                //        }

                //    }


                //    MessageBox.Show("El concepto de ingreso no puede ser eliminado ya que esta asignado a " + ingresosEnConceptoIngreso.Count + " ingreso(s) " +
                //    "con los siguientes datos: \n" + ingresos,
                //        "Error al eliminar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                //else
                //{

                int res = AnalisisBR.eliminar(analisis);

                if (res != 0)
                {
                    MessageBox.Show("El análisis ha sido eliminado.", "Eliminación de análisis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                listar();
                nuevo();
                //}


            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void cbTipoPaciente_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                obtenerPrecio();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbGrupo_SelectionChangeCommitted(object sender, EventArgs e)
        {

            try
            {
                 listar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCategoria_KeyUp_1(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbUnidades_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbValorReferencia_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbTipoPaciente_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbPrecioAnalisis_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
