﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using BusinessLayer;
using Transversal;

namespace AnalisisClinicos
{
    public partial class F0501 : Form
    {
        public F0501()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario();
                usuario.clave = Global.claveUsuario;
                usuario.contrasena = md5(tbContrasenaActual.Text);

                if (tbContrasenaNueva.Text == tbConfirmarContrasenaNueva.Text)
                {

                    if (UsuarioBR.existe(usuario))
                    {
                        usuario.contrasena = md5(tbContrasenaNueva.Text);
                        int res = UsuarioBR.actualizar(usuario);

                        if (res != 0)
                        {
                            MessageBox.Show("La constraseña ha sido actualizado correctamente", "Cambio de contraseña", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                    else
                    {
                        throw new Exception("La contraseña actual es incorrecta");
                    }
                }
                else
                {
                    MessageBox.Show("La confirmación de la contraseña es diferente", "Error en confirmación de contraseña", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static string md5(string Value)
        {
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
                data = x.ComputeHash(data);
                string ret = "";
                for (int i = 0; i < data.Length; i++)
                    ret += data[i].ToString("x2").ToLower();
                return ret;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), "Cambio de contraseña");
                return Value;
            }
        }
    }
}
