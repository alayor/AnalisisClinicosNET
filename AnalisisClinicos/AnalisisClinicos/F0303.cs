﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;

namespace AnalisisClinicos
{
    public partial class F0303 : Form
    {
        BindingList<Analisis_consulta> lsAnalisis;
        static internal int idAnalisis;

        public F0303()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                    obtenerAnalisis();
                    tbNombre.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                idAnalisis = 0;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAnalisis.Rows.Count > 0 && dgvAnalisis.CurrentCell.RowIndex >= 0)
                {

                    idAnalisis = lsAnalisis[dgvAnalisis.CurrentRow.Index].idAnalisis;

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0303_Load(object sender, EventArgs e)
        {
            try
            {
                dgvAnalisis.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular);
                tbNombre.Select();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void obtenerAnalisis()
        {
            try
            {
                if (tbNombre.Text != "" )
                {
                    Analisis_consulta analisis = new Analisis_consulta();
                    analisis.nombre = tbNombre.Text;


                    lsAnalisis = AnalisisBR.listarPorNombre(analisis);

                    FDataGridView.Llenar(dgvAnalisis, lsAnalisis, "idAnalisis",
                    "nombre", "nombreGrupo", "nombreCategoria");

                    dgvAnalisis.Columns["idAnalisis"].HeaderText = "No.";
                    dgvAnalisis.Columns["idAnalisis"].DisplayIndex = 0;
                    dgvAnalisis.Columns["idAnalisis"].Width = Convert.ToInt32(dgvAnalisis.Width * .10);

                    dgvAnalisis.Columns["nombre"].HeaderText = "Nombre";
                    dgvAnalisis.Columns["nombre"].DisplayIndex = 1;
                    dgvAnalisis.Columns["nombre"].Width = Convert.ToInt32(dgvAnalisis.Width * .30);

                    dgvAnalisis.Columns["nombreGrupo"].HeaderText = "Grupo";
                    dgvAnalisis.Columns["nombreGrupo"].DisplayIndex = 2;
                    dgvAnalisis.Columns["nombreGrupo"].Width = Convert.ToInt32(dgvAnalisis.Width * .30);


                    dgvAnalisis.Columns["nombreCategoria"].HeaderText = "Categoria";
                    dgvAnalisis.Columns["nombreCategoria"].DisplayIndex = 3;
                    dgvAnalisis.Columns["nombreCategoria"].Width = Convert.ToInt32(dgvAnalisis.Width * .30);
                }
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }
    }
}
