﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Transversal;
using BusinessLayer;

namespace AnalisisClinicos
{
    public partial class InicioSesion : Form
    {
        public InicioSesion()
        {
            InitializeComponent();
        }

        private void InicioSesion_Load(object sender, EventArgs e)
        {
            tbClave.Text = "Administrador";
            tbContrasena.Select();
        }

        private void bAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario();
                usuario.clave = tbClave.Text;
                Global.claveUsuario = usuario.clave;
                usuario.contrasena = md5(tbContrasena.Text);

                if (UsuarioBR.existe(usuario))
                    this.Close();
                else
                    MessageBox.Show("Usuario o contraseña incorrectos.", "Datos incorrectos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbContrasena.ResetText();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancelar_Click(object sender, EventArgs e)
        {

            Application.ExitThread();
            this.Close();
        }
        private static string md5(string Value)
        {
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
                data = x.ComputeHash(data);
                string ret = "";
                for (int i = 0; i < data.Length; i++)
                    ret += data[i].ToString("x2").ToLower();
                return ret;

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), "Error MD5");
                return "";
            }
        }
    }

}
