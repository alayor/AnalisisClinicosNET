﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;

namespace AnalisisClinicos
{
    public partial class F0401 : Form
    {
        BindingList<ResultadoDetalle_resultadosPorMes> lsResultados;

        public F0401()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0401_Load(object sender, EventArgs e)
        {
            try
            {
                dtpFechaInicio.Value = dtpFechaInicio.Value.AddMonths(-1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpFechaInicio_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                obtenerResultados();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpFechaFin_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                obtenerResultados();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                imprimir();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbConceptoIngreso_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                   
                    obtenerResultados();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbConceptoIngreso_Leave(object sender, EventArgs e)
        {
            try
            {
              
                obtenerResultados();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbConceptoIngreso_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                obtenerResultados();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void obtenerResultados()
        {
            try
            {



                ResultadoDetalle_resultadosPorMes resultado = new ResultadoDetalle_resultadosPorMes();
                resultado.fechaInicio = dtpFechaInicio.Value;
                resultado.fechaFin = dtpFechaFin.Value;


                lsResultados = ResultadoDetalleBR.resultadosPorMes(resultado);
                var lista = (from l in lsResultados select l).Distinct(new ProductComparer());
                BindingList<ResultadoDetalle_resultadosPorMes> listaTemp =new BindingList<ResultadoDetalle_resultadosPorMes>(lista.ToList());

                FDataGridView.Llenar(dgvResultado, listaTemp, "nombrePaciente", "numeroAfiliacion");

                dgvResultado.Columns["nombrePaciente"].DisplayIndex = 0;
                dgvResultado.Columns["nombrePaciente"].HeaderText = "Paciente";
                dgvResultado.Columns["numeroAfiliacion"].HeaderText = "No Afiliacion";


            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }


        private void imprimir()
        {
            try
            {

                Reporte<ResultadoDetalle_resultadosPorMes> reporte = new Reporte<ResultadoDetalle_resultadosPorMes>(lsResultados, @"rpt/CR0401.rpt");

              //  Dictionary<String, String> parametros = new Dictionary<string, string>();


              //  parametros.Add("CLAVE", paciente.clave);
                reporte.Imprimir();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

    }
}
