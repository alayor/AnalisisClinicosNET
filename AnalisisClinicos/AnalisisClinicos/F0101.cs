﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal;
using BusinessLayer;
using Entities;
using Utilities;


namespace AnalisisClinicos
{
    public partial class F0101 : Form
    {

        BindingList<Categoria> lsCategoria;


        public F0101()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void F0101_Load(object sender, EventArgs e)
        {
            try
            {
                dgvCategorias.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular);
                listar();
                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                nuevo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvCategorias_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCategorias.CurrentRow.Index >= 0)
                {
                    tbNumero.Text = lsCategoria[dgvCategorias.CurrentRow.Index].idCategoria.ToString();
                    tbNombre.Text = lsCategoria[dgvCategorias.CurrentRow.Index].nombre;

                    bEliminar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbNumero_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNumero.Text == "0" || tbNumero.Text == "")
                    bGuardar.Text = "&Guardar";
                else
                    bGuardar.Text = "&Actualizar";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                eliminar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Int32.Parse(tbNumero.Text) == 0)
                {
                    guardar();
                }
                else
                {
                    actualizar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void nuevo()
        {
            try
            {

                tbNumero.Text= "0";
                tbNombre.ResetText();
                tbNombre.Select();
                dgvCategorias.ClearSelection();

                bEliminar.Enabled = false;
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }

        }

        private void listar()
        {
            try
            {

                lsCategoria = CategoriaBR.listar();
                FDataGridView.Llenar(dgvCategorias, lsCategoria, "idCategoria", "nombre");
                dgvCategorias.Columns["idCategoria"].HeaderText = "No.";
                dgvCategorias.Columns["idCategoria"].Width = 120;
                dgvCategorias.Columns["nombre"].HeaderText = "Categoria";


                dgvCategorias.ClearSelection();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void guardar()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre de la categoria.", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }

                Categoria categoria = new Categoria();
                categoria.idCategoria = Int32.Parse(tbNumero.Text);
                categoria.nombre = tbNombre.Text;

                CategoriaBR.insertar(categoria);

                if (categoria.idCategoria != 0)
                {
                    MessageBox.Show("La categoria ha sido registrado.", "Registro de categoría", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();
                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void actualizar()
        {
            try
            {

                if (String.IsNullOrWhiteSpace(tbNombre.Text))
                {
                    MessageBox.Show("Ingrese el nombre de la categoria", "Error en registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbNombre.Select();
                    return;
                }

                Categoria categoria = new Categoria();

                categoria.idCategoria = Int32.Parse(tbNumero.Text);
                categoria.nombre = tbNombre.Text;


                int res = CategoriaBR.actualizar(categoria);

                if (res != 0)
                {
                    MessageBox.Show("La categoria ha sido actualizada.", "Actualización de categoria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nuevo();
                }

                listar();
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void eliminar()
        {
            try
            {

                if (MessageBox.Show("¿Desea eliminar la categoria?", "Eliminar categoria", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                Categoria categoria = new Categoria();
                Grupo grupo = new Grupo();

                grupo.idCategoria = Int32.Parse(tbNumero.Text);
                categoria.idCategoria = Int32.Parse(tbNumero.Text);

                BindingList<Grupo> gruposEnCategoria = GrupoBR.listarPorIdCategoria(grupo);

                if (gruposEnCategoria.Count > 0)
                {
                    String grupos = "  Nombre\n";
                    int cont = 0;
                    foreach (Grupo gru in gruposEnCategoria)
                    {
                        grupos += "  " +gru.nombre.ToString() + "\n";
                        cont++;

                        if (cont > 7)
                        {
                            grupos += "Y la lista continua...";
                            break;
                        }

                    }


                    MessageBox.Show("La categoria no puede ser eliminada ya que esta asignado a " + gruposEnCategoria.Count + " grupo(s) " +
                    "con los siguientes datos: \n" + grupos,
                        "Error al eliminar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    int res = CategoriaBR.eliminar(categoria);

                    if (res != 0)
                    {
                        MessageBox.Show("La categoria ha sido eliminada.", "Eliminación de categoria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    listar();
                    nuevo();
                }


            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), this.Text);
            }
        }

        private void tbNombre_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {

                  System.Windows.Forms.SendKeys.Send("{TAB}"); 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        


    }
}
