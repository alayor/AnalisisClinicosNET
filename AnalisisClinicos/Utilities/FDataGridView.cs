﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.ComponentModel;

namespace Utilities
{
    public class FDataGridView
    {
        public static void Llenar<T>(DataGridView dataGridView, IList<T> source, params String[] columnas)
        {

            dataGridView.DataSource = source;

            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                if (!columnas.Contains(column.DataPropertyName))
                {

                    dataGridView.Columns[column.HeaderText].Visible = false;
                }


            }
            dataGridView.Select();
        }
    }
}
