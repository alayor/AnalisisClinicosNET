﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Windows.Forms;
using System.ComponentModel;


namespace Utilities
{
    public class Reporte<T>
    {
        ReportDocument reportDocument = new ReportDocument();

        public Reporte(BindingList<T> listaEntidad, String rutaReporte)
        {
            reportDocument.Load(rutaReporte);
            reportDocument.SetDataSource(listaEntidad);
        }

        public void Imprimir()
        {
            Imprimir(null);
        }


        public void Imprimir(Dictionary<String, String> parametros)
        {
            if (parametros != null)
                llenarParametros(parametros);

            VistaPreviaReporte vistaPrevia = new VistaPreviaReporte();
            vistaPrevia.crvReport.ReportSource = reportDocument;
            vistaPrevia.crvReport.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            vistaPrevia.WindowState = FormWindowState.Maximized;
            vistaPrevia.Show();
        }

        private void llenarParametros(Dictionary<String, String> parametros)
        {

            ParameterFieldDefinitions parameterFieldDefinitions;
            ParameterFieldDefinition parameterFieldDefinition;
            ParameterValues parameterValues;
            ParameterDiscreteValue parameterDiscreteValue;

            parameterFieldDefinitions = reportDocument.DataDefinition.ParameterFields;

            List<String> llaves = new List<String>();
            List<String> valores = new List<String>();

            llaves = parametros.Keys.ToList<string>();
            valores = parametros.Values.ToList<string>();

            for (int i = 0; i < parametros.Count; i++)
            {


                parameterFieldDefinition = reportDocument.DataDefinition.ParameterFields[llaves[i]];

                parameterValues = parameterFieldDefinition.CurrentValues;

                parameterDiscreteValue = new ParameterDiscreteValue();

                parameterDiscreteValue.Value = valores[i];


                parameterValues.Add(parameterDiscreteValue);

                parameterFieldDefinition.ApplyCurrentValues(parameterValues);

            }

        }
    }
}
