﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;

namespace BusinessLayer
{
    public class PrecioAnalisisBR
    {
        static public void insertar(PrecioAnalisis precioAnalisis)
        {
            try
            {
                IPersistence persistencia = new PrecioAnalisisDS();
                precioAnalisis.idAnalisis = persistencia.insertar(precioAnalisis, ProcedimientosAlmacenados.PrecioAnalisis_insertar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), precioAnalisis);
            }
        }

        static public int actualizarPorAnalisiTipoPaciente(PrecioAnalisis precio_analisis)
        {
            try
            {
                IPersistence persistencia = new PrecioAnalisisDS();
                return persistencia.actualizar(precio_analisis, ProcedimientosAlmacenados.PrecioAnalisis_actualizarPorAnalisisTipoPaciente);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), precio_analisis);
                return 0;
            }
        }

        static public bool existePorAnalisisTipoPaciente(PrecioAnalisis precio_analisis)
        {
            try
            {
                IPersistence persistencia = new PrecioAnalisisDS();
                return persistencia.existe(precio_analisis, ProcedimientosAlmacenados.PrecioAnalisis_existePorAnalisisTipoPaciente);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), precio_analisis);
                return false;
            }
        }

    }
}
