﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;

namespace BusinessLayer
{
    public class TipoPacienteBR
    {
        static public void insertar(TipoPaciente tipoPaciente)
        {
            try
            {
                IPersistence persistencia = new TipoPacienteDS();
                tipoPaciente.idTipoPaciente = persistencia.insertar(tipoPaciente, ProcedimientosAlmacenados.TipoPaciente_insertar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), tipoPaciente);
            }
        }

        static public int actualizar(TipoPaciente tipoPaciente)
        {
            try
            {
                IPersistence persistencia = new TipoPacienteDS();
                return persistencia.actualizar(tipoPaciente, ProcedimientosAlmacenados.TipoPaciente_actualizar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), tipoPaciente);
                return 0;
            }
        }

        static public int eliminar(TipoPaciente tipoPaciente)
        {
            try
            {
                IPersistence persistencia = new TipoPacienteDS();
                return persistencia.eliminar(tipoPaciente, ProcedimientosAlmacenados.TipoPaciente_eliminar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), tipoPaciente);
                return 0;
            }
        }

        static public BindingList<TipoPaciente> listar()
        {
            try
            {
                IPersistence persistencia = new TipoPacienteDS();
                return persistencia.consultar(new TipoPaciente(), ProcedimientosAlmacenados.TipoPaciente_listar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), new TipoPaciente());
                return null;
            }
        }


    }
}
