﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;

namespace BusinessLayer
{
    public class AnalisisBR
    {
        static public void insertar(Analisis analisis)
        {
            try
            {
                IPersistence persistencia = new AnalisisDS();
                analisis.idAnalisis = persistencia.insertar(analisis, ProcedimientosAlmacenados.Analisis_insertar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), analisis);
            }
        }


        static public int actualizar(Analisis analisis)
        {
            try
            {
                IPersistence persistencia = new AnalisisDS();
                return persistencia.actualizar(analisis, ProcedimientosAlmacenados.Analisis_actualizar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), analisis);
                return 0;
            }
        }

        static public bool existe(Analisis analisis)
        {
            try
            {
                IPersistence persistencia = new AnalisisDS();
                return persistencia.existe(analisis, ProcedimientosAlmacenados.Analisis_existe);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), analisis);
                return false;
            }
        }

        static public int eliminar(Analisis analisis)
        {
            try
            {
                IPersistence persistencia = new AnalisisDS();
                return persistencia.eliminar(analisis, ProcedimientosAlmacenados.Analisis_eliminar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), analisis);
                return 0;
            }
        }

        static public BindingList<Analisis> listarPorIdGrupo(Analisis analisis)
        {
            try
            {
                IPersistence persistencia = new AnalisisDS();
                return persistencia.consultar(analisis, ProcedimientosAlmacenados.Analisis_listarPorIdGrupo);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), analisis);
                return null;
            }
        }

        static public BindingList<Analisis_consulta> listarPorNombre(Analisis_consulta analisis)
        {
            try
            {
                IPersistence persistencia = new AnalisisDS();
                return persistencia.consultar(analisis, ProcedimientosAlmacenados.Analisis_listarPorNombre);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), analisis);
                return null;
            }
        }
    }
}
