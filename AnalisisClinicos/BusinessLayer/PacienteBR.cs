﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;

namespace BusinessLayer
{
    public class PacienteBR
    {
        static public void insertar(Paciente paciente)
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                paciente.idPaciente = persistencia.insertar(paciente, ProcedimientosAlmacenados.Paciente_insertar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), paciente);
            }
        }

        static public int actualizar(Paciente paciente)
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                return persistencia.actualizar(paciente, ProcedimientosAlmacenados.Paciente_actualizar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), paciente);
                return 0;
            }
        }

        static public bool existe(Paciente paciente)
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                return persistencia.existe(paciente, ProcedimientosAlmacenados.Paciente_existe);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), paciente);
                return false;
            }
        }

        static public int eliminar(Paciente paciente)
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                return persistencia.eliminar(paciente, ProcedimientosAlmacenados.Paciente_eliminar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), paciente);
                return 0;
            }
        }

        static public BindingList<Paciente> listarPorNombreClave(Paciente paciente)
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                return persistencia.consultar(paciente, ProcedimientosAlmacenados.Paciente_listarPorNombreClave);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), paciente);
                return null;
            }
        }

        static public BindingList<Paciente> listar()
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                return persistencia.consultar(new Paciente(), ProcedimientosAlmacenados.Paciente_listar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), new Paciente());
                return null;
            }
        }

        static public bool existePorClave(Paciente paciente)
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                return persistencia.existe(paciente, ProcedimientosAlmacenados.Paciente_existePorClave);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), paciente);
                return false;
            }
        }



        static public BindingList<Paciente> listarPorIdTipoPaciente(Paciente paciente)
        {
            try
            {
                IPersistence persistencia = new PacienteDS();
                return persistencia.consultar(paciente, ProcedimientosAlmacenados.Paciente_listarPorIdTipoPaciente);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), paciente);
                return null;
            }
        }
    }
}
