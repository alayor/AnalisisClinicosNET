﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;
namespace BusinessLayer
{
   public class ResultadoDetalleBR
    {
       static public void insertar(ResultadoDetalle resultado_detalle)
       {
           try
           {
               IPersistence persistencia = new ResultadoDetalleDS();
               resultado_detalle.idResultadoDetalle = persistencia.insertar(resultado_detalle, ProcedimientosAlmacenados.ResultadoDetalle_insertar);
           }
           catch (Exception ex)
           {
               Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado_detalle);
           }
       }

       static public int actualizar(ResultadoDetalle resultado_detalle)
       {
           try
           {
               IPersistence persistencia = new ResultadoDetalleDS();
               return persistencia.actualizar(resultado_detalle, ProcedimientosAlmacenados.ResultadoDetalle_actualizar);

           }
           catch (Exception ex)
           {
               Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado_detalle);
               return 0;
           }
       }

       static public int eliminar(ResultadoDetalle resultado_detalle)
       {
           try
           {
               IPersistence persistencia = new ResultadoDetalleDS();
               return persistencia.eliminar(resultado_detalle, ProcedimientosAlmacenados.ResultadoDetalle_eliminar);
           }
           catch (Exception ex)
           {
               Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado_detalle);
               return 0;
           }
       }

       static public BindingList<ResultadoDetalle> listarPorIdResultado(ResultadoDetalle resultadoDetalle)
       {
           try
           {
               IPersistence persistencia = new ResultadoDetalleDS();
               return persistencia.consultar(resultadoDetalle, ProcedimientosAlmacenados.ResultadoDetalle_listarPorIdResultado);

           }
           catch (Exception ex)
           {
               Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultadoDetalle);
               return null;
           }
       }

       static public BindingList<ResultadoDetalle_resultadosPorMes> resultadosPorMes(ResultadoDetalle_resultadosPorMes resultadoDetalle)
       {
           try
           {
               IPersistence persistencia = new ResultadoDetalleDS();
               return persistencia.consultar(resultadoDetalle, ProcedimientosAlmacenados.ResultadoDetalle_resultados_por_mes);

           }
           catch (Exception ex)
           {
               Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultadoDetalle);
               return null;
           }
       }

       static public BindingList<ResultadoDetalle_consulta> consultar(ResultadoDetalle_consulta resultadoDetalle)
       {
           try
           {
               IPersistence persistencia = new ResultadoDetalleDS();
               return persistencia.consultar(resultadoDetalle, ProcedimientosAlmacenados.ResultadoDetalle_consultar);

           }
           catch (Exception ex)
           {
               Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultadoDetalle);
               return null;
           }
       }
    }
}
