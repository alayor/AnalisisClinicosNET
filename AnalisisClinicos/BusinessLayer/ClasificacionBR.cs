﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;

namespace BusinessLayer
{
    public class CategoriaBR
    {
        static public void insertar(Categoria categoria)
        {
            try
            {
                IPersistence persistencia = new CategoriaDS();
                categoria.idCategoria = persistencia.insertar(categoria, ProcedimientosAlmacenados.Categoria_insertar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), categoria);
            }
        }

        static public int actualizar(Categoria categoria)
        {
            try
            {
                IPersistence persistencia = new CategoriaDS();
                return persistencia.actualizar(categoria, ProcedimientosAlmacenados.Categoria_actualizar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), categoria);
                return 0;
            }
        }

        static public int eliminar(Categoria categoria)
        {
            try
            {
                IPersistence persistencia = new CategoriaDS();
                return persistencia.eliminar(categoria, ProcedimientosAlmacenados.Categoria_eliminar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), categoria);
                return 0;
            }
        }

        static public BindingList<Categoria> listar()
        {
            try
            {
                IPersistence persistencia = new CategoriaDS();
                return persistencia.consultar(new Categoria(), ProcedimientosAlmacenados.Categoria_listar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), new Categoria());
                return null;
            }
        }
    }
}
