﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;

namespace BusinessLayer
{
    public class UsuarioBR
    {
        static public bool existe(Usuario usuario)
        {
            try
            {
                IPersistence persistencia = new UsuarioDS();
                return persistencia.existe(usuario, ProcedimientosAlmacenados.Usuario_existe_por_clave_contrasena);
            }
            catch (Exception ex)
            {
              Transversal.Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), usuario);
                return false;
            }
        }

        static public int actualizar(Usuario usuario)
        {
            try
            {
                IPersistence persistencia = new UsuarioDS();
                return persistencia.actualizar(usuario, ProcedimientosAlmacenados.Usuario_actualizar);
            }
            catch (Exception ex)
            {
                Transversal.Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), usuario);
                return 0;
            }
        }

    }
}
