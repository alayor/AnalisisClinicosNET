﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;


namespace BusinessLayer
{
    public class ResultadoBR
    {
        static public void insertar(Resultado resultado)
        {
            try
            {
                IPersistence persistencia = new ResultadoDS();
                resultado.idResultado = persistencia.insertar(resultado, ProcedimientosAlmacenados.Resultado_insertar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado);
            }
        }

        static public void eliminar(int idResultado)
        {
            try
            {
                IPersistence persistencia = new ResultadoDS();
                Resultado resultado = new Resultado();
                resultado.idResultado = idResultado;
                persistencia.eliminar(resultado, ProcedimientosAlmacenados.Resultado_eliminar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), idResultado);
            }
        }


        static public int actualizar(Resultado resultado)
        {
            try
            {
                IPersistence persistencia = new ResultadoDS();
                return persistencia.actualizar(resultado, ProcedimientosAlmacenados.Resultado_actualizar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado);
                return 0;
            }
        }

        static public BindingList<Resultado_consulta> consultarPorFecha(Resultado_consulta resultado)
        {
            try
            {
                IPersistence persistencia = new ResultadoDS();
                return persistencia.consultar(resultado, ProcedimientosAlmacenados.Resultado_consultar_por_fecha);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado);
                return null;
            }
        }

        static public BindingList<Resultado_consulta> consultarPorFechaClavePaciente(Resultado_consulta resultado)
        {
            try
            {
                IPersistence persistencia = new ResultadoDS();
                return persistencia.consultar(resultado, ProcedimientosAlmacenados.Resultado_consultar_por_fecha_clavePaciente);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado);
                return null;
            }
        }
        
        static public bool existe(Resultado resultado)
        {
            try
            {
                IPersistence persistencia = new ResultadoDS();
                return persistencia.existe(resultado, ProcedimientosAlmacenados.Resultado_existe);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), resultado);
                return false;
            }
        }

    }
}
