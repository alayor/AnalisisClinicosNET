﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DataAccessLayer;
using Transversal;
using System.ComponentModel;

namespace BusinessLayer
{
    public class GrupoBR
    {
        static public void insertar(Grupo grupo)
        {
            try
            {
                IPersistence persistencia = new GrupoDS();
                grupo.idGrupo = persistencia.insertar(grupo, ProcedimientosAlmacenados.Grupo_insertar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), grupo);
            }
        }

        static public int actualizar(Grupo grupo)
        {
            try
            {
                IPersistence persistencia = new GrupoDS();
                return persistencia.actualizar(grupo, ProcedimientosAlmacenados.Grupo_actualizar);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), grupo);
                return 0;
            }
        }

        static public bool existe(Grupo grupo)
        {
            try
            {
                IPersistence persistencia = new GrupoDS();
                return persistencia.existe(grupo, ProcedimientosAlmacenados.Grupo_existe);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), grupo);
                return false;
            }
        }

        static public int eliminar(Grupo grupo)
        {
            try
            {
                IPersistence persistencia = new GrupoDS();
                return persistencia.eliminar(grupo, ProcedimientosAlmacenados.Grupo_eliminar);
            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(), grupo);
                return 0;
            }
        }

        static public BindingList<Grupo> listarPorIdCategoria(Grupo grupo)
        {
            try
            {
                IPersistence persistencia = new GrupoDS();
                return persistencia.consultar(grupo, ProcedimientosAlmacenados.Grupo_listarPorIdCategoria);

            }
            catch (Exception ex)
            {
                Excepcion.ManejarExcepcion(ex, System.Reflection.MethodBase.GetCurrentMethod(),grupo);
                return null;
            }
        }
    }
}
