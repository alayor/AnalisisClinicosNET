﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class Resultado_consulta:Resultado
    {
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }

        public String ToString()
        {
            return base.ToString() +
                "fechaInicio: " + fechaInicio + " - " +
                "fechaFin" + fechaFin;
        }
    }
}
