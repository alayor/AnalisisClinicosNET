﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Categoria
    {
        public int idCategoria { get; set; }
        public String nombre { get; set; }

        public override String ToString()
        {
            return "idCategoria: " + idCategoria + " - " +
                "nombre:" + nombre + " - ";
        }
    }
}
