﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Analisis_consulta : Analisis
    {
        public String nombreGrupo { get; set; }
        public String nombreCategoria { get; set; }

        public String ToString()
        {
            return base.ToString()+
                "nombreGrupo: " + nombreGrupo + " - "+
                "nombreCategoria" + nombreCategoria;
        }
    }
}
