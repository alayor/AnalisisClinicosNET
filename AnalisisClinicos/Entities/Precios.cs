﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Precios
    {


        private String analisis { get; set; }
        private String precio { get; set; }


       public Precios(String analisis, String precio)
        {
            this.analisis = analisis;
            this.precio = precio;
        }

       public override String ToString()
       {
           return  analisis + " - " + precio;
       }
    }
}
