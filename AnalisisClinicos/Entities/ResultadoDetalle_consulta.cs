﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class ResultadoDetalle_consulta:ResultadoDetalle
    {
        public String nombrePaciente { get; set; }
        public String unidades { get; set; }
        public String valorReferencia { get; set; }
        public String nombreGrupo { get; set; }
        public String nombreCategoria { get; set; }

        public String ToString()
        {
            return base.ToString() +
                "fechaInombrePacientenicio: " + nombrePaciente + " - " +
                "unidades: " + unidades + " - " +
                "valorReferencia: " + valorReferencia + " - " +
                "nombreGrupo: " + nombreGrupo + " - " +
                "nombreCategoria" + nombreCategoria;
        }
    }
}
