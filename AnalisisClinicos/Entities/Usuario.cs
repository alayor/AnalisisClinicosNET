﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Usuario
    {

        public int idUsuario { get; set; }
        public String clave { get; set; }
        public String contrasena { get; set; }
        public String nombre { get; set; }
        public int idGrupo { get; set; }
        public int idEstado { get; set; }

        public override String ToString()
        {
            return "idUsuario: " + idUsuario + " - " +
                "clave:" + clave + " - " +
                "contrasena:" + contrasena + " - " +
                "nombre:" + nombre + " - " +
                "idGrupo:" + idGrupo + " - " +
                "idEstado:" + idEstado + " - ";
        }

    }
}
