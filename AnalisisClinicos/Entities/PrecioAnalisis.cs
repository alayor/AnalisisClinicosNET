﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class PrecioAnalisis
    {
        public int idPrecioAnalisis { get; set; }
        public int idAnalisis { get; set; }
        public int idTipoPaciente { get; set; }
        public decimal precio { get; set; }

        public override String ToString()
        {
            return "idPrecioAnalisis: " + idPrecioAnalisis + " - " +
                "idAnalisis: " + idAnalisis + " - " +
                "idTipoPaciente: " + idTipoPaciente + " - " +
                "precio:" + precio;
        }
    }
}
