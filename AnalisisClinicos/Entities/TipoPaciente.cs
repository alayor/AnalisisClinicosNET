﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TipoPaciente
    {
        public int idTipoPaciente { get; set; }
        public String nombre { get; set; }

        public override String ToString()
        {
            return "idTipoPaciente: " + idTipoPaciente + " - " +
                "nombre:" + nombre + " - ";
        }
    }
}
