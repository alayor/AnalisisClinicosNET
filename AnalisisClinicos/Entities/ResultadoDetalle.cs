﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class ResultadoDetalle
    {

        public int idResultadoDetalle { get; set; }
        public int idResultado { get; set; }
        public int idAnalisis { get; set; }
        public String nombreAnalisis { get; set; }
        public String resultado { get; set; }

        public override String ToString()
        {
            return "idResultadoDetalle: " + idResultadoDetalle + " - " +
                  "idResultado: " + idResultado + " - " +
                   "idAnalisis: " + idAnalisis + " - " +
                   "nombreAnalisis: " + nombreAnalisis + " - " +
                    "resultado: " + resultado + " - ";
        }


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ResultadoDetalle p = obj as ResultadoDetalle;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (idAnalisis == p.idAnalisis) ;
        }

        public bool Equals(ResultadoDetalle p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (idAnalisis == p.idAnalisis);
        }

        public override int GetHashCode()
        {
            return idAnalisis ^ idAnalisis;
        }
    }
}
