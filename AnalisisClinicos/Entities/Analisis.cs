﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Analisis
    {
        public int idAnalisis { get; set; }
        public int idGrupo { get; set; }
        public String nombre { get; set; }
        public String unidades { get; set; }
        public String valorReferencia { get; set; }

        public override String ToString()
        {
            return "idAnalisis: " + idAnalisis + " - " +
                "idGrupo: " + idGrupo + " - " +
                "nombre: " + nombre + " - " +
                "unidades: " + unidades + " - " +
                "valorReferencia:" + valorReferencia;
        }
    }
}
