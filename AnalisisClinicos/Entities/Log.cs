﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Log
    {
        public int idLog { get; set; }
        public String descripcion { get; set; }
        public String clase { get; set; }
        public String metodo { get; set; }
        public String entidad { get; set; }
        public DateTime fecha { get; set; }


        public Log(Exception excepcion, System.Reflection.MethodBase metodo, Object entidad)
        {
            this.descripcion = excepcion.Message;
            this.clase = metodo.DeclaringType.Name;
            this.metodo = metodo.Name;
            this.entidad = entidad.ToString();
            this.fecha = DateTime.Now;
        }

        public override String ToString()
        {
            return "idLog: " + idLog + " - " +
                "descripcion:" + descripcion + " - " +
                "clase:" + clase + " - " +
                "metodo:" + metodo + " - " +
                "entidad:" + entidad + " - " +
                "fecha:" + fecha + " - ";
        }
    }
}
