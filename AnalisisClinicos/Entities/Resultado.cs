﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Resultado
    {
        public int idResultado { get; set; }
        public int idPaciente { get; set; }
        public String nombrePaciente { get; set; }
        public DateTime fechaCreacion { get; set; }
        public DateTime fechaModificacion { get; set; }
        public String clavePaciente { get; set; }


        public override String ToString()
        {
            return "idResultado: " + idResultado + " - " +
                  "idPaciente: " + idPaciente + " - " +
                   "nombrePaciente: " + nombrePaciente + " - " +
                   "fechaCreacion: " + fechaCreacion + " - " +
                   "fechaModificacion: " + fechaModificacion + " - " +
                    "clavePaciente: " + clavePaciente + " - ";
        }
    }
}
