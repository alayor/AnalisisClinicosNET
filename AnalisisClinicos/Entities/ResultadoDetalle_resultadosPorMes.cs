﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class ResultadoDetalle_resultadosPorMes:ResultadoDetalle_consulta
    {
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public String numeroAfiliacion { get; set; }
        public decimal precio { get; set; }

        public String ToString()
        {
            return base.ToString() +
                "fechaInicio: " + fechaInicio + " - " +
                "numeroAfiliacion: " + numeroAfiliacion + " - " +
                "precio: " + precio + " - " +
                "fechaFin" + fechaFin;
        }
    }


  public class ProductComparer : IEqualityComparer<ResultadoDetalle_resultadosPorMes>
   {
       // Products are equal if their names and product numbers are equal.
       public bool Equals(ResultadoDetalle_resultadosPorMes x, ResultadoDetalle_resultadosPorMes y)
       {

           //Check whether the compared objects reference the same data.
           if (Object.ReferenceEquals(x, y)) return true;

           //Check whether any of the compared objects is null.
           if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
               return false;

           //Check whether the products' properties are equal.
           return x.nombrePaciente == y.nombrePaciente;
       }

       // If Equals() returns true for a pair of objects 
       // then GetHashCode() must return the same value for these objects.

       public int GetHashCode(ResultadoDetalle_resultadosPorMes product)
       {
           //Check whether the object is null
           if (Object.ReferenceEquals(product, null)) return 0;

           //Get hash code for the Name field if it is not null.
           int hashProductName = product.nombrePaciente == null ? 0 : product.nombrePaciente.GetHashCode();

           //Get hash code for the Code field.
           int hashProductCode = product.nombrePaciente.GetHashCode();

           //Calculate the hash code for the product.
           return hashProductName ^ hashProductCode;
       }

   }

}
