﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class Paciente
    {
        public int idPaciente { get; set; }
        public String clave { get; set; }
        public int idTipoPaciente { get; set; }
        public String nombre { get; set; }
        public String apellidoPaterno { get; set; }
        public String apellidoMaterno { get; set; }
        public String telefono { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public String genero { get; set; }
        private DateTime _fechaIngreso;
        public DateTime fechaIngreso { get; set; }
        public String numeroAfiliacion { get; set; }

        public override String ToString()
        {
            return "idPaciente: " + idPaciente + " - " +
                 "clave: " + clave + " - " +
                 "idTipoPaciente: " + idTipoPaciente + " - " +
                "nombre: " + nombre + " - " +
                 "apellidoPaterno: " + apellidoPaterno + " - " +
                  "apellidoMaterno: " + apellidoMaterno + " - " +
                "telefono: " + telefono + " - " +
                "fechaNacimiento: " + fechaNacimiento + " - " +
                "genero: " + genero + " - " +
                "fechaIngreso: " + fechaIngreso + " - " +
                "numeroAfiliacion:" + numeroAfiliacion;
        }
    }
}
