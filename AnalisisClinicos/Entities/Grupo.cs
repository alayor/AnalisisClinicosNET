﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Grupo
    {
        public int idGrupo { get; set; }
        public int idCategoria { get; set; }
        public String nombre { get; set; }

        public override String ToString()
        {
            return "idGrupo: " + idGrupo + " - " +
                "idCategoria: " + idCategoria + " - " +
                "nombre:" + nombre ;
        }
    }
}
