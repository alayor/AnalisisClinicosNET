﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer
{
    public class LogDS
    {
        static public int insertar<T>(T entidad)
        {
            int retVal = 0;
            try
            {

                MySqlHelper msh = MySqlHelper.getMySqlHelper(MySqlHelper.ConnectionList.AuditoriaSindical);

                retVal = (int)msh.executeCommand(entidad, "Log_insertar", MySqlHelper.ExecuteType.NonQuery);


            }
            catch (Exception)
            {

            }

            return retVal;
        }
    }
}
