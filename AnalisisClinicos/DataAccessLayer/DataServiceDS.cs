﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;

namespace DataAccessLayer
{
    public class DataServiceDS : IPersistence
    {
        private SqlHelper sh;

        public int insertar<T>(T entidad, String procedimientoAlmacenado)
        {
            int retVal = 0;

            SqlHelper msh = MySqlHelper.getMySqlHelper(MySqlHelper.ConnectionList.AuditoriaSindical);

            retVal = (int)msh.executeCommand(entidad, procedimientoAlmacenado, MySqlHelper.ExecuteType.NonQuery);


            return retVal;
        }

        public bool existe<T>(T entidad, String procedimientoAlmacenado)
        {
            bool retVal = false;

            SqlHelper msh = MySqlHelper.getMySqlHelper(MySqlHelper.ConnectionList.AuditoriaSindical);

            int res = (int)msh.executeCommand(entidad, procedimientoAlmacenado, MySqlHelper.ExecuteType.NonQuery);

            retVal = res == 1 ? true : false;

            if (res == 1) msh.fillEntity(entidad);

            return retVal;

        }

        public BindingList<T> consultar<T>(T entidad, String procedimientoAlmacenado)
        {
            SqlHelper msh = MySqlHelper.getMySqlHelper(MySqlHelper.ConnectionList.AuditoriaSindical);

            return (BindingList<T>)msh.executeCommand(entidad, procedimientoAlmacenado, MySqlHelper.ExecuteType.Query);

        }

        public int actualizar<T>(T entidad, String procedimientoAlmacenado)
        {
            SqlHelper msh = MySqlHelper.getMySqlHelper(MySqlHelper.ConnectionList.AuditoriaSindical);

            return (int)msh.executeCommand(entidad, procedimientoAlmacenado, MySqlHelper.ExecuteType.NonQuery);
        }

        public int eliminar<T>(T entidad, String procedimientoAlmacenado)
        {
            SqlHelper msh = MySqlHelper.getMySqlHelper(MySqlHelper.ConnectionList.AuditoriaSindical);

            return (int)msh.executeCommand(entidad, procedimientoAlmacenado, MySqlHelper.ExecuteType.NonQuery);
        }

        public void abrirTransaccion()
        {
            sh = MySqlHelper.getMySqlHelper(MySqlHelper.ConnectionList.AuditoriaSindical);

            sh.openTransaction();
        }

        public void cerrarTransaccion()
        {
            sh.closeTransaction();
        }
    }
}
