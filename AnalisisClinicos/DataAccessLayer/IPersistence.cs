﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DataAccessLayer
{
    public interface IPersistence
    {
        int insertar<T>(T entidad, String procedimientoAlmacenado);

        bool existe<T>(T entidad, String procedimientoAlmacenado);

        BindingList<T> consultar<T>(T entidad, String procedimientoAlmacenado);

        int actualizar<T>(T entidad, String procedimientoAlmacenado);

        int eliminar<T>(T entidad, String procedimientoAlmacenado);

        void abrirTransaccion();

        void cerrarTransaccion();
    }
}
