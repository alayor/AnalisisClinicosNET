﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.IO;
namespace DataAccessLayer
{
    public abstract class SqlHelper
    {


        public enum ConnectionList
        {
            AuditoriaSindical
        }

        public enum ExecuteType
        {
            Query,
            NonQuery
        }

        protected abstract String readConnectionString();

        protected virtual void testConnection()
        {
            openConnection();
            closeConnection();
        }

        protected abstract void openConnection();

        protected abstract void closeConnection();

        public abstract void openTransaction();

        public abstract void closeTransaction();

        public abstract Object executeCommand<T>(T Entity, String nameSP, ExecuteType type);

        protected abstract void fillParameters<T>(T Entity);

        public abstract void fillEntity<T>(T Entity);

    }
}
