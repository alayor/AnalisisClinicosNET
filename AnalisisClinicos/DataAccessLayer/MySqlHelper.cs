﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.IO;
using System.ComponentModel;

namespace DataAccessLayer
{
    public class MySqlHelper : SqlHelper
    {

        private static MySqlHelper msh = null;

        private static MySqlConnection conn;

        private static MySqlCommand cmd;

        private static MySqlTransaction tran = null;


        public MySqlHelper(ConnectionList connectionName)
        {
            if (conn == null)
            {
                conn = new MySqlConnection();
            }

            conn.ConnectionString = readConnectionString();
            testConnection();
        }

        public override void openTransaction()
        {
            tran = conn.BeginTransaction();
            tran = null;
        }

        public override void closeTransaction()
        {
            tran.Commit();
            closeConnection();
            tran = null;
        }

        protected override String readConnectionString()
        {

            StreamReader sr = new StreamReader("./AC.ini");

            try
            {
                return sr.ReadLine();
            }
            finally
            {
                sr.Close();
            }
        }

        private MySqlHelper() { }

        public static MySqlHelper getMySqlHelper(ConnectionList connectionName)
        {

            if (msh == null)
            {
                msh = new MySqlHelper(connectionName);
            }

            return msh;
        }

        protected override void testConnection()
        {

            openConnection();
            closeConnection();

        }

        protected override void openConnection()
        {
            conn.Open();

        }

        protected override void closeConnection()
        {
            if (conn.State != ConnectionState.Closed)
            {
                conn.Close();
            }

        }

        public override Object executeCommand<T>(T Entity, String nameSP, ExecuteType type)
        {

            Object retVal = null;
            try
            {


                openConnection();

                cmd = new MySqlCommand(nameSP, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                fillParameters(Entity);

                switch (type)
                {
                    case ExecuteType.NonQuery:
                        {
                            cmd.ExecuteNonQuery();
                            retVal = cmd.Parameters["@res"].Value; break;
                        }

                    case ExecuteType.Query:
                        retVal = convertDataReaderToEntities(cmd.ExecuteReader(), Entity); break;

                    default: retVal = null; break;

                }
            }
            catch (Exception ex)
            {
                if (tran != null)
                {
                    tran.Rollback();
                    tran = null;
                    closeConnection();
                }
                throw ex;
            }


            finally
            {
                if (tran == null)
                {
                    closeConnection();
                }
            }
            return retVal;
        }

        protected override void fillParameters<T>(T Entity)
        {

            foreach (PropertyInfo p in Entity.GetType().GetProperties())
            {
                cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(Entity, null));
                cmd.Parameters["@" + p.Name].Direction = ParameterDirection.InputOutput;
            }
            cmd.Parameters.AddWithValue("@res", 0);
            cmd.Parameters["@res"].Direction = ParameterDirection.Output;


        }

        public override void fillEntity<T>(T Entity)
        {

            foreach (MySqlParameter p in cmd.Parameters)
            {
                if (p.ParameterName != "@res")
                    Entity.GetType().GetProperty(p.ParameterName.Substring(1)).SetValue(Entity, p.Value, null);
            }

        }

        private static DataTable convertDataReaderToDataSet(MySqlDataReader reader)
        {
            DataSet dataSet = new DataSet();
            do
            {
                // Create new data table

                DataTable schemaTable = reader.GetSchemaTable();
                DataTable dataTable = new DataTable();

                if (schemaTable != null)
                {
                    // A query returning records was executed

                    for (int i = 0; i < schemaTable.Rows.Count; i++)
                    {
                        DataRow dataRow = schemaTable.Rows[i];
                        // Create a column name that is unique in the data table
                        string columnName = (string)dataRow["ColumnName"]; //+ "<C" + i + "/>";
                        // Add the column definition to the data table
                        DataColumn column = new DataColumn(columnName, (Type)dataRow["DataType"]);
                        dataTable.Columns.Add(column);
                    }

                    dataSet.Tables.Add(dataTable);

                    // Fill the data table we just created

                    while (reader.Read())
                    {
                        DataRow dataRow = dataTable.NewRow();

                        for (int i = 0; i < reader.FieldCount; i++)
                            dataRow[i] = reader.GetValue(i);

                        dataTable.Rows.Add(dataRow);
                    }
                }
                else
                {
                    // No records were returned

                    DataColumn column = new DataColumn("RowsAffected");
                    dataTable.Columns.Add(column);
                    dataSet.Tables.Add(dataTable);
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = reader.RecordsAffected;
                    dataTable.Rows.Add(dataRow);
                }
            }
            while (reader.NextResult());

            return dataSet.Tables[0];
        }

        private static BindingList<T> convertDataReaderToEntities<T>(MySqlDataReader reader, T entityType)
        {
            BindingList<T> objectList = new BindingList<T>();
            DataTable schemaTable = reader.GetSchemaTable();
            T item;

            while (reader.Read())
            {
                item = Activator.CreateInstance<T>();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    entityType.GetType().GetProperty(reader.GetSchemaTable().Rows[i]["ColumnName"].ToString()).SetValue(item, reader.GetValue(i), null);
                }

                objectList.Add(item);
            }
            return objectList;
        }
    }
}
