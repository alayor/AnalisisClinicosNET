﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
using Entities;
using System.Net.Mail;
using Utilities;
using System.IO;

namespace Transversal
{
    public class Excepcion : Exception
    {
        private String mensaje = "Ha ocurrido un error en el sistema, " +
            "favor de repotarlo con el administrador del sistema proporcionándole el siguiente codigo: AC_";

        private static int idLog;

        public override string Message
        {
            get { return mensaje; }
        }

        private Excepcion()
        {
            mensaje += idLog;

        }

        public static void ManejarExcepcion(Exception ex, System.Reflection.MethodBase metodo, Object entidad)
        {
            if (ex is Excepcion)
            {
                throw ex;
            }
            else
            {
                Log log = new Log(ex, metodo, entidad);
                log.idLog = LogDS.insertar(log);
                idLog = log.idLog;
                enviarCorreo(log.ToString());
                escribirLog(log.ToString());
                throw new Excepcion();

            }
        }

        private static void enviarCorreo(String mensaje)
        {
            try
            {

                Correo Cr = new Correo();

                MailMessage mnsj = new MailMessage();

                mnsj.Subject = "AS" + idLog;

                mnsj.To.Add(new MailAddress("alayor3@gmail.com"));
                mnsj.To.Add(new MailAddress("hdezgilberto@hotmail.com"));

                mnsj.From = new MailAddress("alayor3@gmail.com", "SistemaAS");

                // mnsj.Attachments.Add(new Attachment("C:\\archivo.pdf"));

                mnsj.Body = mensaje;

                Cr.MandarCorreo(mnsj);

            }
            catch (Exception)
            {

            }
        }

        private static void escribirLog(String mensaje)
        {
            using (StreamWriter sw = File.AppendText("./log.txt"))
            {
                sw.WriteLine(mensaje);
            }
           
        }
    }
}
